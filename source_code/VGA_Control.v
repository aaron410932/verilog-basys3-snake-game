// according to the object(body, background, wall) that current scanning, output corresponding color
module VGA_Control
(
	input clk,
	input rst,
	input start,
	input special,
	input hsync_i,
    input vsync_i,
    input [9:0] h_cnt,
    input [9:0] v_cnt,
    input [11:0] pixel,
	input [1:0] game_status,
	input [2:0]snake,
	input [2:0]snake_p2,
	input [5:0]apple_x,
	input [4:0]apple_y,
	input wallmode,
	input player_num_state,
	output reg[9:0]x_pos,
	output reg[9:0]y_pos,	
	output reg hsync,
	output reg vsync,
	output reg [11:0] color_out
);

	reg [19:0]clk_cnt;
	reg [9:0]line_cnt;
	reg clk_25M;
	
	localparam NONE = 3'b000;
	localparam HEAD = 3'b001;
	localparam BODY = 3'b010;
	localparam WALL = 3'b011;
	
	//localparam HEAD_COLOR = 12'b0000_1111_0000;
	localparam BODY_COLOR = 12'b0110_0110_1111;
	localparam HEAD_COLOR = 12'b0100_0100_1111;
	
	reg [3:0]lox;
	reg [3:0]loy;
	reg flag = 1'b0;
	always@(posedge clk or negedge rst) begin
		if(rst) begin
			clk_cnt <= 0;
			line_cnt <= 0;
			hsync <= 1;
			vsync <= 1;
			flag <= 1'b0;
		end
		else if(game_status == 2'b1 && flag == 1'b0)begin
		    if(wallmode == 1) begin
		        if(h_cnt > 182 && h_cnt < 260 && v_cnt > 342 && v_cnt < 411) 
		            color_out = 12'd0;
		        else begin
		            if(player_num_state == 1'b1) begin
		                if(h_cnt > 315 && h_cnt < 342 && v_cnt > 420 && v_cnt < 455) 
		                    color_out = 12'b0;
		                else
		                    color_out = {pixel[3:0], pixel[7:4], pixel[11:8]};
		            end
		            else begin
		                if(h_cnt > 215 && h_cnt < 242 && v_cnt > 420 && v_cnt < 455) 
		                    color_out = 12'b0;
		                else
		                    color_out = {pixel[3:0], pixel[7:4], pixel[11:8]};
		            end
		        end
		    end
		    else begin
		        if(h_cnt > 182 && h_cnt < 260 && v_cnt > 274 && v_cnt < 342) 
		            color_out = 12'd0;
		        else begin
		            if(player_num_state == 1'b1) begin
		                if(h_cnt > 315 && h_cnt < 342 && v_cnt > 420 && v_cnt < 455) 
		                    color_out = 12'b0;
		                else
		                    color_out = {pixel[3:0], pixel[7:4], pixel[11:8]};
		            end
		            else begin
		                if(h_cnt > 215 && h_cnt < 242 && v_cnt > 420 && v_cnt < 455) 
		                    color_out = 12'b0;
		                else
		                    color_out = {pixel[3:0], pixel[7:4], pixel[11:8]};
		            end
		        end
		    end
		    
		    hsync = hsync_i;
		    vsync = vsync_i;
		    if(start == 1'b1) begin
		        flag <= 1'b1;
		    end
		end
		else begin
		    if(game_status == 2'b11) begin
		      flag <= 1'b0;
		    end
		    x_pos <= clk_cnt - 144;
			y_pos <= line_cnt - 33;	
			if(clk_cnt == 0) begin
			    hsync <= 0;
				clk_cnt <= clk_cnt + 1;
            end
			else if(clk_cnt == 96) begin
				hsync <= 1;
				clk_cnt <= clk_cnt + 1;
            end
			else if(clk_cnt == 799) begin
				clk_cnt <= 0;
				line_cnt <= line_cnt + 1;
			end
			else clk_cnt <= clk_cnt + 1;
			if(line_cnt == 0) begin
				vsync <= 0;
            end
			else if(line_cnt == 2) begin
				vsync <= 1;
			end
			else if(line_cnt == 521) begin
				line_cnt <= 0;
				vsync <= 0;
			end
			
			if(x_pos >= 0 && x_pos < 640 && y_pos >= 0 && y_pos < 480) begin
			    lox = x_pos[3:0];
				loy = y_pos[3:0];						
				if(x_pos[9:4] == apple_x && y_pos[9:4] == apple_y)
					case({loy,lox})
						8'b0000_0000:color_out = 12'b0000_0000_0000;
						default: begin
						    if(special == 1'b1)
						      color_out = 12'b1001_0010_1111;
						    else
						      color_out = 12'b1111_1100_1100;					    
						end
					endcase						
				
				else if(snake == HEAD|snake == BODY) begin   //detect the point belong to which object and output corresponding color
					case({lox,loy})
						8'b0000_0000:color_out = 12'b0000_0000_0000;
						default:color_out = (snake == HEAD) ?  HEAD_COLOR : BODY_COLOR;
					endcase
				end
				else if(player_num_state == 0 && (snake_p2 == HEAD | snake_p2 == BODY)) begin   //detect the point belong to which object and output corresponding color
					case({lox,loy})
						8'b0000_0000:color_out = 12'b0000_0000_0000;
						default:color_out = (snake_p2 == HEAD) ?  12'b1111_1111_0000 : 12'b1111_1001_1000;
					endcase
				end
				else if(snake == NONE)
					color_out = 12'b0000_0000_0000;
				else if(snake == WALL)
					color_out = 3'b101;
			end
		    else
			    color_out = 12'b0000_0000_0000;
		end
    end
endmodule
