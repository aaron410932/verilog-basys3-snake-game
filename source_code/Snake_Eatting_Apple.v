//generate apple

module Snake_Eatting_Apple
(
	input clk,
	input rst,
	input timeup,
	input [5:0]head_x,
	input [5:0]head_y,
	input [5:0]head_x_p2,
	input [5:0]head_y_p2,
	output reg [5:0]apple_x,
	output reg [4:0]apple_y,
	output reg add_cube,
	output reg add_cube_p2,
	output reg special
);

	reg [31:0]clk_cnt;
	reg [10:0]random_num;
	reg [15:0] myled;
	always@(posedge clk)
		random_num <= random_num + 1111;  //use add to random 
		//random_num[10:5] is x position
		//random_num[4:0]  is y position
	
	always@(posedge clk or negedge rst) begin
		if(!rst) begin
			clk_cnt <= 0;
			apple_x <= 24;
			apple_y <= 10;
			add_cube <= 0;
			add_cube_p2 <= 0;
			myled <= 16'b0;
		end
		else begin
			clk_cnt <= clk_cnt+1;
			if(clk_cnt == 250_000) begin
				clk_cnt <= 0;
				if(apple_x == head_x && apple_y == head_y) begin
					add_cube <= 1;
					apple_x <= (random_num[10:5] > 38) ? (random_num[10:5] - 25) : (random_num[10:5] == 0) ? 1 : random_num[10:5];
					apple_y <= (random_num[4:0] > 28) ? (random_num[4:0] - 3) : (random_num[4:0] == 0) ? 1:random_num[4:0];
					if(random_num[4:0] % 5 == 1 && special != 1'b1) begin
					   special <= 1;
					end
					else begin
					   special <= 0;
					end
				end   //detect whether the random position exceed the boundary
				else if(apple_x == head_x_p2 && apple_y == head_y_p2) begin
					add_cube_p2 <= 1;
					apple_x <= (random_num[10:5] > 38) ? (random_num[10:5] - 25) : (random_num[10:5] == 0) ? 1 : random_num[10:5];
					apple_y <= (random_num[4:0] > 28) ? (random_num[4:0] - 3) : (random_num[4:0] == 0) ? 1:random_num[4:0];
					if(random_num[4:0] % 5 == 1 && special != 1'b1) begin
					   special <= 1;
					end
					else begin
					   special <= 0;
					end
				end
				else if(timeup == 1'b1) begin
				    apple_x <= (random_num[10:5] > 38) ? (random_num[10:5] - 25) : (random_num[10:5] == 0) ? 1 : random_num[10:5];
					apple_y <= (random_num[4:0] > 28) ? (random_num[4:0] - 3) : (random_num[4:0] == 0) ? 1:random_num[4:0];
					special <= 0;
				end
				else begin
					add_cube <= 0;
					add_cube_p2 <= 0;
			    end
			end
		end
	end
	
endmodule

