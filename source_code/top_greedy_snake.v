//top module
`define INIT 3'd0
`define SPE  3'd1
`define NSPE 3'd2

`define P1 1'd1
`define P2 1'd0

module top_greedy_snake
(
    input clk,
	input rst,
	input speed1,
	input start,
	input yeswall,
	input nowall,
	input player_num,
    inout PS2_DATA,
    inout PS2_CLK,
	output hsync,
	output vsync,
	output [11:0]color_out,//{VgaBlue, VgaGreen, VgaRed}
	output [7:0]seg_out,
	output reg [15:0] led = 16'b0,
	output [3:0]sel,
	output audio_mclk, // master clock
    output audio_lrck, // left-right clock
    output audio_sck, // serial clock
    output audio_sdin // serial audio data input
);

	wire left1, w1;
	wire right1, a1;
	wire up1, s1;
	wire down1,d1;
	wire w_press, a_press, s_press, d_press;
	wire left_key_press;
	wire right_key_press;
	wire up_key_press;
	wire down_key_press;
	wire [2:0]snake;
	wire [2:0]snake_p2;
	wire [9:0]x_pos;
	wire [9:0]y_pos;
	wire [5:0]apple_x;
	wire [4:0]apple_y;
	wire [5:0]head_x;
	wire [5:0]head_y;
	wire [5:0]head_x_p2;
	wire [5:0]head_y_p2;
	wire add_cube;
	wire add_cube_p2;
	wire[1:0]game_status;
	wire[1:0]game_status_p2;
	wire hit_wall;
	wire hit_wall_p2;
	wire hit_body;
	wire hit_body_p2;
	wire die_flash;
	wire die_flash_p2;
	wire restart;
	wire restart_p2;
	wire [6:0]cube_num;
	wire [6:0]cube_num_p2;
	wire rst_n;
	wire [2:0]statew;
	
	wire p1_hit_p2;
	wire p2_hit_p1;
	wire clk16;
	wire opyeswall;
	wire opnowall;
	wire opplayer;
	wire special;
	wire timeup;
	reg timeupr = 1'b0;
	assign rst_n = ~rst;
	
    clock_divider #(.n(16)) clock_16(
        .clk(clk),
        .clk_div(clk16)
    );

    wire clk24;
	clock_divider #(.n(24)) clock_24(
        .clk(clk),
        .clk_div(clk24)
    );
    reg player_num_state = `P1;
    reg [1:0]num_flag = 2'b0;
    always@(posedge clk16) begin
       if(rst) begin
	       player_num_state <= `P1;
	   end
	   else begin
	       if(game_status == 2'b01) begin
	           if(opplayer == 1'b1 && num_flag == 2'b0) begin
	               player_num_state <= ~player_num_state;
	               num_flag <= 2'b1;
	           end
	           else begin
	               if(num_flag != 2'b0) begin
	                   num_flag <= num_flag + 2'b1;
	               end
	           end
	       end
	   end
    end
    reg [2:0] state = `NSPE;
	always@(posedge clk24) begin
	   if(rst) begin
	       led <= 16'b0;
	       state <= `NSPE;
	       timeupr = 1'b0;
	   end
	   case(state)
	       `SPE: begin
	           if(special == 0) begin 
	               state <= `NSPE;
	               led <= 16'b0;
	               timeupr = 0;
	           end
	           else if(led > 0)
	               led <= led >> 1;
	           else
	               timeupr <= 1;
	           
	       end
	       `NSPE: begin
	           if(special == 1) begin
	               state <= `SPE;
	               led <= 16'b1111_1111_1111_1111;
	           end
	           timeupr = 0;
	       end
	   endcase
	end
	assign timeup = timeupr;
    OnePulse opyes(opyeswall, yeswall, clk16);
    OnePulse opno(opnowall, nowall, clk16);
    OnePulse oppla(opplayer, player_num, clk16);
    speaker_top speak1(
        .clk(clk), // clock from crystal
        .rst(rst), // active high reset: BTNC
        .game_status(game_status),
        .audio_mclk(audio_mclk), // master clock
        .audio_lrck(audio_lrck), // left-right clock
        .audio_sck(audio_sck), // serial clock
        .audio_sdin(audio_sdin) // serial audio data input
    );
    Game_Ctrl_Unit U1 (
        .num(1),
        .clk(clk),
	    .rst(rst_n),
	    .key1_press(left_key_press),
	    .key2_press(right_key_press),
	    .key3_press(up_key_press),
	    .key4_press(down_key_press),
	    .player_num_state(player_num_state),
        .game_status(game_status),
		.hit_wall(hit_wall),
		.hit_body(hit_body),
		.p1_hit_p2(p1_hit_p2),
		.p2_hit_p1(p2_hit_p1),
		.hit_wall_other(hit_wall_p2),
		.hit_body_other(hit_body_p2),
		.die_flash(die_flash),
		.restart(restart)		
	);
	Game_Ctrl_Unit P2G (
        .num(0),
        .clk(clk),
	    .rst(rst_n),
	    .key1_press(a_press),
	    .key2_press(d_press),
	    .key3_press(w_press),
	    .key4_press(s_press),
	    .player_num_state(player_num_state),
        .game_status(game_status_p2),
		.hit_wall(hit_wall_p2),
		.hit_body(hit_body_p2),
		.p1_hit_p2(p1_hit_p2),
		.p2_hit_p1(p2_hit_p1),
		.hit_wall_other(hit_wall),
		.hit_body_other(hit_body),
		.die_flash(die_flash_p2),
		.restart(restart_p2)		
	);
	Snake_Eatting_Apple U2 (
        .clk(clk),
		.rst(rst_n),
		.timeup(timeup),
		.apple_x(apple_x),
		.apple_y(apple_y),
		.head_x(head_x),
		.head_y(head_y),
		.head_x_p2(head_x_p2),
		.head_y_p2(head_y_p2),
		.add_cube(add_cube),
		.add_cube_p2(add_cube_p2),
		.special(special)	
	);
	Transition t1(
    .left(left1),
	.right(right1),
	.up(up1),
	.down(down1),
	.w1(w1),
	.a1(a1),
	.s1(s1),
	.d1(d1),
	.PS2_DATA(PS2_DATA),
	.PS2_CLK(PS2_CLK),
	.rst(rst),
	.clk(clk)
	);
	wire wallmode;
	Snake U3 (
	    .clk(clk),
		.rst(rst_n),
		.truerst(rst),
		.speed1(speed1),
		.yeswall(opyeswall),
		.nowall(opnowall),
		.left_press(left_key_press),
		.right_press(right_key_press),
		.up_press(up_key_press),
		.down_press(down_key_press),
		.other_head_x(head_x_p2),
	    .other_head_y(head_y_p2),
	    .otherhit(p2_hit_p1),
	    .player_num_state(player_num_state),
		.snake(snake),
		.x_pos(x_pos),//receive x,y postion from VGA, and decide the object now scanning
		.y_pos(y_pos),
		.head_x(head_x),
		.head_y(head_y),
		.add_cube(add_cube),
		.game_status(game_status),
		.cube_num(cube_num),
		.hit_body(hit_body),
		.hit_wall(hit_wall),
		.die_flash(die_flash),
		.wallmode(wallmode)
	);
	Snake_p2 U33 (
	    .clk(clk),
		.rst(rst_n),
		.truerst(rst),
		.speed1(speed1),
		.yeswall(opyeswall),
		.nowall(opnowall),
		.left_press(a_press),
		.right_press(d_press),
		.up_press(w_press),
		.down_press(s_press),
		.other_head_x(head_x),
	    .other_head_y(head_y),
	    .otherhit(p1_hit_p2),
	    .player_num_state(player_num_state),
		.snake(snake_p2),
		.x_pos(x_pos),//receive x,y postion from VGA, and decide the object now scanning
		.y_pos(y_pos),
		.head_x(head_x_p2),
		.head_y(head_y_p2),
		.add_cube(add_cube_p2),
		.game_status(game_status),
		.cube_num(cube_num_p2),
		.hit_body(hit_body_p2),
		.hit_wall(hit_wall_p2),
		.die_flash(die_flash_p2)
	);
    reg hsync_i,vsync_i;
    wire [11:0] pixel;
    wire [9:0] h_cnt;  //640
    wire [9:0] v_cnt;  //480
	VGA_top U4 (
		.clk(clk),
		.rst(rst),
		.start(start),
		.game_status(game_status),
		.hsync_i(hsync_i),
		.vsync_i(vsync_i),
		.wallmode(wallmode),
		.player_num_state(player_num_state),
		.pixel(pixel),
		.hsync(hsync),
		.vsync(vsync),
		.h_cnt(h_cnt),
		.v_cnt(v_cnt),
		.special(special),
		.snake(snake),//Snake module will output snake(object that scan) for VGA_control
		.snake_p2(snake_p2),
        .color_out(color_out),
		.x_pos(x_pos),
		.y_pos(y_pos),//decide x,y position here for Snake module
		.apple_x(apple_x),
		.apple_y(apple_y)
	);
	
	Key U5 (
		.clk(clk),
		.rst(rst_n),
		.left(left1),
		.right(right1),
		.up(up1),
		.down(down1),
		.left_key_press(left_key_press),
		.right_key_press(right_key_press),
		.up_key_press(up_key_press),
		.down_key_press(down_key_press)		
	);
	Key U66 (
		.clk(clk),
		.rst(rst_n),
		.left(a1),
		.right(d1),
		.up(w1),
		.down(s1),
		.left_key_press(a_press),
		.right_key_press(d_press),
		.up_key_press(w_press),
		.down_key_press(s_press)		
	);
	assign statew = state;
	Seg_Display U6 (
		.clk(clk),
		.rst(rst_n),	
		.state(statew),
		.add_cube(add_cube),
		.game_status(game_status),
		.seg_out(seg_out),
		.sel(sel)	
	);
	
	
	wire [11:0] data;
    wire clk_25MHz;
    wire clk_22;
    wire [16:0] pixel_addr;
    
    wire valid;
	clock_divisor clk_wiz_0_inst(
      .clk(clk),
      .clk1(clk_25MHz),
      .clk22(clk_22)
    );

    mem_addr_gen mem_addr_gen_inst(
    .clk(clk_22),
    .rst(rst),
    .h_cnt(h_cnt),
    .v_cnt(v_cnt),
    .pixel_addr(pixel_addr)
    );
     
 
    blk_mem_gen_0 blk_mem_gen_0_inst(
      .clka(clk_25MHz),
      .wea(0),
      .addra(pixel_addr),
      .dina(data[11:0]),
      .douta(pixel)
    );
     reg [9:0]pixel_cnt;
    reg [9:0]line_cnt;
    //reg hsync_i,vsync_i;

    parameter HD = 640;
    parameter HF = 16;
    parameter HS = 96;
    parameter HB = 48;
    parameter HT = 800; 
    parameter VD = 480;
    parameter VF = 10;
    parameter VS = 2;
    parameter VB = 33;
    parameter VT = 525;
    parameter hsync_default = 1'b1;
    parameter vsync_default = 1'b1;

    always @(posedge clk_25MHz)
        if (rst)
            pixel_cnt <= 0;
        else
            if (pixel_cnt < (HT - 1))
                pixel_cnt <= pixel_cnt + 1;
            else
                pixel_cnt <= 0;

    always @(posedge clk_25MHz)
        if (rst)
            hsync_i <= hsync_default;
        else
            if ((pixel_cnt >= (HD + HF - 1)) && (pixel_cnt < (HD + HF + HS - 1)))
                hsync_i <= ~hsync_default;
            else
                hsync_i <= hsync_default; 

    always @(posedge clk_25MHz)
        if (rst)
            line_cnt <= 0;
        else
            if (pixel_cnt == (HT -1))
                if (line_cnt < (VT - 1))
                    line_cnt <= line_cnt + 1;
                else
                    line_cnt <= 0;

    always @(posedge clk_25MHz)
        if (rst)
            vsync_i <= vsync_default; 
        else if ((line_cnt >= (VD + VF - 1)) && (line_cnt < (VD + VF + VS - 1)))
            vsync_i <= ~vsync_default; 
        else
            vsync_i <= vsync_default; 

    assign valid = ((pixel_cnt < HD) && (line_cnt < VD));

    assign h_cnt = (pixel_cnt < HD) ? pixel_cnt : 10'd0;
    assign v_cnt = (line_cnt < VD) ? line_cnt : 10'd0;
endmodule

module mem_addr_gen(
   input clk,
   input rst,
   input [9:0] h_cnt,
   input [9:0] v_cnt,
   output [16:0] pixel_addr
   );
    
   reg [7:0] position;
  
   assign pixel_addr = ((h_cnt>>1)+320*(v_cnt>>1))% 76800;  //640*480 --> 320*240 



endmodule

module clock_divider(clk, clk_div);   
    parameter n = 26;     
    input clk;   
    output clk_div;   
    
    reg [n-1:0] num;
    wire [n-1:0] next_num;
    
    always@(posedge clk)begin
    	num<=next_num;
    end
    
    assign next_num = num +1;
    assign clk_div = num[n-1];
    
endmodule

module OnePulse (
	output reg signal_single_pulse,
	input wire signal,
	input wire clock
	);
	
	reg signal_delay;

	always @(posedge clock) begin
		if (signal == 1'b1 & signal_delay == 1'b0)
		  signal_single_pulse <= 1'b1;
		else
		  signal_single_pulse <= 1'b0;

		signal_delay <= signal;
	end
endmodule
module clock_divisor(clk1, clk, clk22);
input clk;
output clk1;
output clk22;
reg [21:0] num;
wire [21:0] next_num;

always @(posedge clk) begin
  num <= next_num;
end

assign next_num = num + 1'b1;
assign clk1 = num[1];
assign clk22 = num[21];
endmodule
