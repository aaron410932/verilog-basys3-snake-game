//according to game status, generate corresponding signal
module Game_Ctrl_Unit
(
	input num,
	input clk,
	input rst,
	input key1_press,
	input key2_press,
	input key3_press,
	input key4_press,
	input p1_hit_p2,
    input p2_hit_p1,
    input player_num_state,
	output reg [1:0]game_status,
	input hit_wall,
	input hit_body,
	input hit_wall_other,
	input hit_body_other,
	output reg die_flash,
	output reg restart		
);
	
	localparam RESTART = 2'b00;
	localparam START = 2'b01;
	localparam PLAY = 2'b10;
	localparam DIE = 2'b11;
	
	reg[31:0]clk_cnt;
	reg ismedie = 1'b0;
	always@(posedge clk or negedge rst)
	begin
		if(!rst) begin
			game_status <= START;
			clk_cnt <= 0;
			die_flash <= 1;
			restart <= 0;
			ismedie <= 1'b0;
		end
		else begin
			case(game_status)			
				RESTART:begin           //waiting for start
					ismedie <= 1'b0;
					if(clk_cnt <= 5) begin
						clk_cnt <= clk_cnt + 1;
						restart <= 1;						
					end
					else begin
						game_status <= START;
						clk_cnt <= 0;
						restart <= 0;
					end
				end
				START:begin
					if (key1_press | key2_press | key3_press | key4_press)
                        game_status <= PLAY;
					else 
					    game_status <= START;
				end
				PLAY:begin
					if(hit_wall | hit_body | hit_wall_other | hit_body_other) begin
					   if(hit_wall | hit_body)
					       ismedie <= 1'b1;
					   game_status <= DIE;
					end
					else if(player_num_state == 0 && (p1_hit_p2 | p2_hit_p1)) begin
					   if(num == 1 && p1_hit_p2 == 1) begin
					       ismedie <= 1'b1;
					   end
					   if(num == 0 && p2_hit_p1 == 1) begin
					       ismedie <= 1'b1;
					   end
					   game_status <= DIE;
					end
					else
					   game_status <= PLAY;
				end					
				DIE:begin
					if(clk_cnt <= 200_000_000) begin
						clk_cnt <= clk_cnt + 1'b1;
					   if(ismedie == 1'b1) begin	
					       if(clk_cnt == 25_000_000)
					           die_flash <= 0;
					       else if(clk_cnt == 50_000_000)
					           die_flash <= 1'b1;
					       else if(clk_cnt == 75_000_000)
					           die_flash <= 1'b0;
					       else if(clk_cnt == 100_000_000)
					           die_flash <= 1'b1;
					       else if(clk_cnt == 125_000_000)
					           die_flash <= 1'b0;
					       else if(clk_cnt == 150_000_000)
					           die_flash <= 1'b1;
					   end
				    end                        
					else
						begin
							die_flash <= 1;
							clk_cnt <= 0;
							game_status <= RESTART;
						end
					end
			endcase
		end
	end
endmodule