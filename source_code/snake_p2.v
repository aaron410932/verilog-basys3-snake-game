//control snake moving
module Snake_p2
(
	input clk,
	input rst,
	input truerst,
	input speed1,
	input yeswall,
	input nowall,
	input left_press,
	input right_press,
	input up_press,
	input down_press,
	input [5:0]other_head_x,
	input [5:0]other_head_y,
	input player_num_state,
	output reg [2:0]snake,//current object, 00:non   01:head   10:body   11:wall
	output reg otherhit,
	input [9:0]x_pos,
	input [9:0]y_pos,//unit: pixel
	
	output [5:0]head_x,	
	output [5:0]head_y,//head cordinate
	
	input add_cube,//add body length signal
	
	input [1:0]game_status,//4 game status
	
	output reg [6:0]cube_num,
	output reg hit_body,   //signal
	output reg hit_wall,   //signal
	input die_flash        //signal
);
	
	localparam UP = 2'b00;
	localparam DOWN = 2'b01;
	localparam LEFT = 2'b10;
	localparam RIGHT = 2'b11;
	
	localparam NONE = 3'b000;
	localparam HEAD = 3'b001;
	localparam BODY = 3'b010;
	localparam WALL = 3'b011;
	
    localparam RESTART = 2'b00;
	localparam START = 2'b01;
	localparam PLAY = 2'b10;
	localparam DIE = 2'b11;
	
	reg[31:0]cnt;
	wire[1:0]direct;
	
	
	
	reg [1:0]direct_r;     //storage direction
	assign direct = direct_r;//stroage next direction
	reg[1:0]direct_next;
	
	reg change_to_left;
	reg change_to_right;
	reg change_to_up;
	reg change_to_down;
	
	reg [5:0]cube_x[50:0];//hint: 16 varbles, each varble has 6bits
	reg [5:0]cube_y[50:0];//length's cordinate, unit:blocks
	reg [50:0]is_exist;    //if is_exist[k] == 1 means snake's length at least k
	
	reg addcube_state;
	
	
	reg iswall = 1'b1;
	wire[35:0] sec_move;  //ex. 0.02us*12'500'000=0.25s  each sec move 4
	assign sec_move = (speed1 == 1'b1)? 12_500_000: 6_250_000;
	
	always@(posedge clk) begin
	   if(game_status == START) begin
	       if(yeswall == 1'b1) begin
	           iswall = 1'b1;
	       end
	       if(nowall == 1'b1) begin
	           iswall = 1'b0;
	       end
	   end
	   if(truerst == 1'b1) begin
	       iswall = 1'b1;
	   end
	end
	
	assign head_x = cube_x[0];
	assign head_y = cube_y[0]; 
	
	always @(posedge clk or negedge rst) begin		
		if(!rst)
			direct_r <= RIGHT; //default direction is right
		else if(game_status == RESTART) 
		    direct_r <= RIGHT;
		else
			direct_r <= direct_next;
	end

    
	always @(posedge clk or negedge rst) begin
		if(!rst) begin
			cnt <= 0;
			//iswall <= 1'b1;					
			cube_x[0] <= 10;
			cube_y[0] <= 20;
					
			cube_x[1] <= 9;
			cube_y[1] <= 20;
					
			cube_x[2] <= 8;
			cube_y[2] <= 20;

			cube_x[3] <= 0;
			cube_y[3] <= 0;
					
			cube_x[4] <= 0;
			cube_y[4] <= 0;
					
			cube_x[5] <= 0;
			cube_y[5] <= 0;
					
			cube_x[6] <= 0;
			cube_y[6] <= 0;
					
			cube_x[7] <= 0;
			cube_y[7] <= 0;
					
			cube_x[8] <= 0;
			cube_y[8] <= 0;
					
			cube_x[9] <= 0;
			cube_y[9] <= 0;					
					
			cube_x[10] <= 0;
			cube_y[10] <= 0;
					
			cube_x[11] <= 0;
			cube_y[11] <= 0;
					
            cube_x[12] <= 0;
			cube_y[12] <= 0;
					
			cube_x[13] <= 0;
			cube_y[13] <= 0;
					
			cube_x[14] <= 0;
			cube_y[14] <= 0;
					
			cube_x[15] <= 0;
			cube_y[15] <= 0;
			
			cube_x[16] <= 0;
			cube_y[16] <= 0;
			
			cube_x[17] <= 0;
			cube_y[17] <= 0;
			
			cube_x[18] <= 0;
			cube_y[18] <= 0;
			
			cube_x[19] <= 0;
			cube_y[19] <= 0;
			
			cube_x[20] <= 0;
			cube_y[20] <= 0;
			
			cube_x[21] <= 0;
			cube_y[21] <= 0;
			
			cube_x[22] <= 0;
			cube_y[22] <= 0;
			
			cube_x[23] <= 0;
			cube_y[23] <= 0;
			
			cube_x[24] <= 0;
			cube_y[24] <= 0;
			
			cube_x[25] <= 0;
			cube_y[25] <= 0;			

			cube_x[26] <= 0;
			cube_y[26] <= 0;
					
			cube_x[27] <= 0;
			cube_y[27] <= 0;
					
			cube_x[28] <= 0;
			cube_y[28] <= 0;
					
			cube_x[29] <= 0;
			cube_y[29] <= 0;
					
			cube_x[30] <= 0;
			cube_y[30] <= 0;
					
			cube_x[31] <= 0;
			cube_y[31] <= 0;
					
			cube_x[32] <= 0;
			cube_y[32] <= 0;					
					
			cube_x[33] <= 0;
			cube_y[33] <= 0;
					
			cube_x[34] <= 0;
			cube_y[34] <= 0;
					
            cube_x[35] <= 0;
			cube_y[35] <= 0;
				
			cube_x[36] <= 0;
			cube_y[36] <= 0;
					
			cube_x[37] <= 0;
			cube_y[37] <= 0;
					
			cube_x[38] <= 0;
			cube_y[38] <= 0;
			
			cube_x[39] <= 0;
			cube_y[39] <= 0;
			
			cube_x[40] <= 0;
			cube_y[40] <= 0;
			
			cube_x[41] <= 0;
			cube_y[41] <= 0;
			
			cube_x[42] <= 0;
			cube_y[42] <= 0;
			
			cube_x[43] <= 0;
			cube_y[43] <= 0;
			
			cube_x[44] <= 0;
			cube_y[44] <= 0;
			
			cube_x[45] <= 0;
			cube_y[45] <= 0;
			
			cube_x[46] <= 0;
			cube_y[46] <= 0;
			
			cube_x[47] <= 0;
			cube_y[47] <= 0;
			
			cube_x[48] <= 0;
			cube_y[48] <= 0;

            cube_x[49] <= 0;
			cube_y[49] <= 0;
			
			cube_x[50] <= 0;
			cube_y[50] <= 0;
			otherhit <= 0;
			hit_wall <= 0;
			hit_body <= 0;//start with length 3, max length is 16,
			              //while rst, output lenght is 3
		end		
		else if(game_status == RESTART) begin
                    cnt <= 0;
                    otherhit <= 0;                                
                    cube_x[0] <= 10;
                    cube_y[0] <= 20;
                                        
                    cube_x[1] <= 9;
                    cube_y[1] <= 20;
                                        
                    cube_x[2] <= 8;
                    cube_y[2] <= 20;
                    
                    cube_x[3] <= 0;
                    cube_y[3] <= 0;
                                        
                    cube_x[4] <= 0;
                    cube_y[4] <= 0;
                                        
                    cube_x[5] <= 0;
                    cube_y[5] <= 0;
                                        
                    cube_x[6] <= 0;
                    cube_y[6] <= 0;
                                        
                    cube_x[7] <= 0;
                    cube_y[7] <= 0;
                                        
                    cube_x[8] <= 0;
                    cube_y[8] <= 0;
                                        
                    cube_x[9] <= 0;
                    cube_y[9] <= 0;
                                        
                    cube_x[10] <= 0;
                    cube_y[10] <= 0;
                                        
                    cube_x[11] <= 0;
                    cube_y[11] <= 0;
                                        
                    cube_x[12] <= 0;
                    cube_y[12] <= 0;
                                        
                    cube_x[13] <= 0;
                    cube_y[13] <= 0;
                                        
                    cube_x[14] <= 0;
                    cube_y[14] <= 0;
                                        
                    cube_x[15] <= 0;
                    cube_y[15] <= 0;
                                  	
			        cube_x[16] <= 0;
			        cube_y[16] <= 0;
			        
			        cube_x[17] <= 0;
			        cube_y[17] <= 0;
			        
			        cube_x[18] <= 0;
			        cube_y[18] <= 0;
			        
			        cube_x[19] <= 0;
			        cube_y[19] <= 0;
			        
			        cube_x[20] <= 0;
			        cube_y[20] <= 0;
			        
			        cube_x[21] <= 0;
			        cube_y[21] <= 0;
			        
			        cube_x[22] <= 0;
			        cube_y[22] <= 0;
			        
			        cube_x[23] <= 0;
			        cube_y[23] <= 0;
			        
			        cube_x[24] <= 0;
			        cube_y[24] <= 0;
			        
			        cube_x[25] <= 0;
			        cube_y[25] <= 0;
			                
			        cube_x[26] <= 0;
			        cube_y[26] <= 0;
			        		
			        cube_x[27] <= 0;
			        cube_y[27] <= 0;
			        		
			        cube_x[28] <= 0;
			        cube_y[28] <= 0;
			        		
			        cube_x[29] <= 0;
			        cube_y[29] <= 0;
			        		
			        cube_x[30] <= 0;
			        cube_y[30] <= 0;
			        		
			        cube_x[31] <= 0;
			        cube_y[31] <= 0;
			        		
			        cube_x[32] <= 0;
			        cube_y[32] <= 0;					
			        		
			        cube_x[33] <= 0;
			        cube_y[33] <= 0;
			        		
			        cube_x[34] <= 0;
			        cube_y[34] <= 0;
			        		
                    cube_x[35] <= 0;
			        cube_y[35] <= 0;
			        	
			        cube_x[36] <= 0;
			        cube_y[36] <= 0;
			        		
			        cube_x[37] <= 0;
			        cube_y[37] <= 0;
			        		
			        cube_x[38] <= 0;
			        cube_y[38] <= 0;
			        
			        cube_x[39] <= 0;
			        cube_y[39] <= 0;
			        
			        cube_x[40] <= 0;
			        cube_y[40] <= 0;
			        
			        cube_x[41] <= 0;
			        cube_y[41] <= 0;
			        
			        cube_x[42] <= 0;
			        cube_y[42] <= 0;
			        
			        cube_x[43] <= 0;
			        cube_y[43] <= 0;
			        
			        cube_x[44] <= 0;
			        cube_y[44] <= 0;
			        
			        cube_x[45] <= 0;
			        cube_y[45] <= 0;
			        
			        cube_x[46] <= 0;
			        cube_y[46] <= 0;
			        
			        cube_x[47] <= 0;
			        cube_y[47] <= 0;
			        
			        cube_x[48] <= 0;
			        cube_y[48] <= 0;

                    cube_x[49] <= 0;
			        cube_y[49] <= 0;
			        
			        cube_x[50] <= 0;
			        cube_y[50] <= 0;
			        
                    hit_wall <= 0;
                    hit_body <= 0;//start with length 3, max length is 16,
			                      //while rst, output lenght is 3                                
        end
        
		else begin
			cnt <= cnt + 1;
			if(cnt >= sec_move) begin   //0.02us*12'500'000=0.25s  each sec move 4
				cnt <= 0;
				if(game_status == PLAY && !player_num_state) begin
					if(iswall == 1'b1 && ((direct == UP && cube_y[0] == 1)|(direct == DOWN && cube_y[0] == 28)|(direct == LEFT && cube_x[0] == 1)|(direct == RIGHT && cube_x[0] == 38)))
					   hit_wall <= 1;//detect hit wall
					else if((cube_y[0] == cube_y[1] && cube_x[0] == cube_x[1] && is_exist[1] == 1)|
							(cube_y[0] == cube_y[2] && cube_x[0] == cube_x[2] && is_exist[2] == 1)|
							(cube_y[0] == cube_y[3] && cube_x[0] == cube_x[3] && is_exist[3] == 1)|
							(cube_y[0] == cube_y[4] && cube_x[0] == cube_x[4] && is_exist[4] == 1)|
							(cube_y[0] == cube_y[5] && cube_x[0] == cube_x[5] && is_exist[5] == 1)|
							(cube_y[0] == cube_y[6] && cube_x[0] == cube_x[6] && is_exist[6] == 1)|
							(cube_y[0] == cube_y[7] && cube_x[0] == cube_x[7] && is_exist[7] == 1)|
							(cube_y[0] == cube_y[8] && cube_x[0] == cube_x[8] && is_exist[8] == 1)|
							(cube_y[0] == cube_y[9] && cube_x[0] == cube_x[9] && is_exist[9] == 1)|
							(cube_y[0] == cube_y[10] && cube_x[0] == cube_x[10] && is_exist[10] == 1)|
							(cube_y[0] == cube_y[11] && cube_x[0] == cube_x[11] && is_exist[11] == 1)|
							(cube_y[0] == cube_y[12] && cube_x[0] == cube_x[12] && is_exist[12] == 1)|
							(cube_y[0] == cube_y[13] && cube_x[0] == cube_x[13] && is_exist[13] == 1)|
							(cube_y[0] == cube_y[14] && cube_x[0] == cube_x[14] && is_exist[14] == 1)|
							(cube_y[0] == cube_y[15] && cube_x[0] == cube_x[15] && is_exist[15] == 1))
							hit_body <= 1;//dectect head position with every body position, if it's same
							              //and this body exist, collision
				    else if((cube_y[0] == cube_y[16] && cube_x[0] == cube_x[16] && is_exist[16] == 1)|
							(cube_y[0] == cube_y[17] && cube_x[0] == cube_x[17] && is_exist[17] == 1)|
							(cube_y[0] == cube_y[18] && cube_x[0] == cube_x[18] && is_exist[18] == 1)|
							(cube_y[0] == cube_y[19] && cube_x[0] == cube_x[19] && is_exist[19] == 1)|
							(cube_y[0] == cube_y[20] && cube_x[0] == cube_x[20] && is_exist[20] == 1)|
							(cube_y[0] == cube_y[21] && cube_x[0] == cube_x[21] && is_exist[21] == 1)|
							(cube_y[0] == cube_y[22] && cube_x[0] == cube_x[22] && is_exist[22] == 1)|
							(cube_y[0] == cube_y[23] && cube_x[0] == cube_x[23] && is_exist[23] == 1)|
							(cube_y[0] == cube_y[24] && cube_x[0] == cube_x[24] && is_exist[24] == 1)|
							(cube_y[0] == cube_y[25] && cube_x[0] == cube_x[25] && is_exist[25] == 1)|
							(cube_y[0] == cube_y[26] && cube_x[0] == cube_x[26] && is_exist[26] == 1)|
							(cube_y[0] == cube_y[27] && cube_x[0] == cube_x[27] && is_exist[27] == 1)|
							(cube_y[0] == cube_y[28] && cube_x[0] == cube_x[28] && is_exist[28] == 1)|
							(cube_y[0] == cube_y[29] && cube_x[0] == cube_x[29] && is_exist[29] == 1)|
							(cube_y[0] == cube_y[30] && cube_x[0] == cube_x[30] && is_exist[30] == 1)|
							(cube_y[0] == cube_y[31] && cube_x[0] == cube_x[31] && is_exist[31] == 1)|
							(cube_y[0] == cube_y[32] && cube_x[0] == cube_x[32] && is_exist[32] == 1)|
							(cube_y[0] == cube_y[33] && cube_x[0] == cube_x[33] && is_exist[33] == 1)|
							(cube_y[0] == cube_y[34] && cube_x[0] == cube_x[34] && is_exist[34] == 1)|
							(cube_y[0] == cube_y[35] && cube_x[0] == cube_x[35] && is_exist[35] == 1))
							hit_body <= 1;
					
					else if((cube_y[0] == cube_y[36] && cube_x[0] == cube_x[36] && is_exist[36] == 1)|
							(cube_y[0] == cube_y[37] && cube_x[0] == cube_x[37] && is_exist[37] == 1)|
							(cube_y[0] == cube_y[38] && cube_x[0] == cube_x[38] && is_exist[38] == 1)|
							(cube_y[0] == cube_y[39] && cube_x[0] == cube_x[39] && is_exist[39] == 1)|
							(cube_y[0] == cube_y[40] && cube_x[0] == cube_x[40] && is_exist[40] == 1)|
							(cube_y[0] == cube_y[41] && cube_x[0] == cube_x[41] && is_exist[41] == 1)|
							(cube_y[0] == cube_y[42] && cube_x[0] == cube_x[42] && is_exist[42] == 1)|
							(cube_y[0] == cube_y[43] && cube_x[0] == cube_x[43] && is_exist[43] == 1)|
							(cube_y[0] == cube_y[44] && cube_x[0] == cube_x[44] && is_exist[44] == 1)|
							(cube_y[0] == cube_y[45] && cube_x[0] == cube_x[45] && is_exist[45] == 1)|
							(cube_y[0] == cube_y[46] && cube_x[0] == cube_x[46] && is_exist[46] == 1)|
							(cube_y[0] == cube_y[47] && cube_x[0] == cube_x[47] && is_exist[47] == 1)|
							(cube_y[0] == cube_y[48] && cube_x[0] == cube_x[48] && is_exist[48] == 1)|
							(cube_y[0] == cube_y[49] && cube_x[0] == cube_x[49] && is_exist[49] == 1)|
							(cube_y[0] == cube_y[50] && cube_x[0] == cube_x[50] && is_exist[50] == 1))
							hit_body <= 1;
							
					else begin
						cube_x[1] <= cube_x[0];
						cube_y[1] <= cube_y[0];
										
						cube_x[2] <= cube_x[1];
						cube_y[2] <= cube_y[1];
										
						cube_x[3] <= cube_x[2];
						cube_y[3] <= cube_y[2];
										
						cube_x[4] <= cube_x[3];
						cube_y[4] <= cube_y[3];
										
						cube_x[5] <= cube_x[4];
						cube_y[5] <= cube_y[4];
										
						cube_x[6] <= cube_x[5];
						cube_y[6] <= cube_y[5];
										
						cube_x[7] <= cube_x[6];
						cube_y[7] <= cube_y[6];
										
						cube_x[8] <= cube_x[7];
						cube_y[8] <= cube_y[7];
										
						cube_x[9] <= cube_x[8];
						cube_y[9] <= cube_y[8];
										
						cube_x[10] <= cube_x[9];
						cube_y[10] <= cube_y[9];
										
						cube_x[11] <= cube_x[10];
						cube_y[11] <= cube_y[10];
										
						cube_x[12] <= cube_x[11];
						cube_y[12] <= cube_y[11];
										 
						cube_x[13] <= cube_x[12];
						cube_y[13] <= cube_y[12];
										
						cube_x[14] <= cube_x[13];
						cube_y[14] <= cube_y[13];
										
						cube_x[15] <= cube_x[14];
						cube_y[15] <= cube_y[14];
						
						cube_x[16] <= cube_x[15];
						cube_y[16] <= cube_y[15];
						
						cube_x[17] <= cube_x[16];
						cube_y[17] <= cube_y[16];
						
						cube_x[18] <= cube_x[17];
						cube_y[18] <= cube_y[17];
						
						cube_x[19] <= cube_x[18];
						cube_y[19] <= cube_y[18];
						
						cube_x[20] <= cube_x[19];
						cube_y[20] <= cube_y[19];
						
						cube_x[21] <= cube_x[20];
						cube_y[21] <= cube_y[20];
						
						cube_x[22] <= cube_x[21];
						cube_y[22] <= cube_y[21];
						
						cube_x[23] <= cube_x[22];
						cube_y[23] <= cube_y[22];
						
						cube_x[24] <= cube_x[23];
						cube_y[24] <= cube_y[23];
						
						cube_x[25] <= cube_x[24];
						cube_y[25] <= cube_y[24];
						
						cube_x[26] <= cube_x[25];
						cube_y[26] <= cube_y[25];
										
						cube_x[27] <= cube_x[26];
						cube_y[27] <= cube_y[26];
										
						cube_x[28] <= cube_x[27];
						cube_y[28] <= cube_y[27];
										
						cube_x[29] <= cube_x[28];
						cube_y[29] <= cube_y[28];
										
						cube_x[30] <= cube_x[29];
						cube_y[30] <= cube_y[29];
										
						cube_x[31] <= cube_x[30];
						cube_y[31] <= cube_y[30];
										
						cube_x[32] <= cube_x[31];
						cube_y[32] <= cube_y[31];
										
						cube_x[33] <= cube_x[32];
						cube_y[33] <= cube_y[32];
										
						cube_x[34] <= cube_x[33];
						cube_y[34] <= cube_y[33];
										
						cube_x[35] <= cube_x[34];
						cube_y[35] <= cube_y[34];
										
						cube_x[36] <= cube_x[35];
						cube_y[36] <= cube_y[35];
										
						cube_x[37] <= cube_x[36];
						cube_y[37] <= cube_y[36];
										 
						cube_x[38] <= cube_x[37];
						cube_y[38] <= cube_y[37];
										
						cube_x[39] <= cube_x[38];
						cube_y[39] <= cube_y[38];
										
						cube_x[40] <= cube_x[39];
						cube_y[40] <= cube_y[39];
						
						cube_x[41] <= cube_x[40];
						cube_y[41] <= cube_y[40];
						
						cube_x[42] <= cube_x[41];
						cube_y[42] <= cube_y[41];
						
						cube_x[43] <= cube_x[42];
						cube_y[43] <= cube_y[42];
						
						cube_x[44] <= cube_x[43];
						cube_y[44] <= cube_y[43];
						
						cube_x[45] <= cube_x[44];
						cube_y[45] <= cube_y[44];
						
						cube_x[46] <= cube_x[45];
						cube_y[46] <= cube_y[45];
						
						cube_x[47] <= cube_x[46];
						cube_y[47] <= cube_y[46];
						
						cube_x[48] <= cube_x[47];
						cube_y[48] <= cube_y[47];
						
						cube_x[49] <= cube_x[48];
						cube_y[49] <= cube_y[48];
						
						cube_x[50] <= cube_x[49];
						cube_y[50] <= cube_y[49];
						//for each body(block), the next position is the prev body
						case(direct)							
							UP: begin
							    if(iswall == 1'b1 && cube_y[0] == 1)
									hit_wall <= 1;
								else begin
								    if(cube_y[0] == 1) begin
								       cube_y[0] <= 28; 
								    end
								    else begin
									   cube_y[0] <= cube_y[0]-1;
									end
							    end
							end
									
							DOWN: begin
								if(iswall == 1'b1 && cube_y[0] == 28)
									hit_wall <= 1;
								else begin
								    if(cube_y[0] == 28) begin
								       cube_y[0] <= 1; 
								    end
								    else begin
									   cube_y[0] <= cube_y[0] + 1;
									end
								end
							end
										
							LEFT: begin
								if(iswall == 1'b1 && cube_x[0] == 1)
									hit_wall <= 1;
								else begin
								    if(cube_x[0] == 1) begin
								       cube_x[0] <= 38; 
								    end
								    else begin
									   cube_x[0] <= cube_x[0]-1;
									end
									
								end											
							end

							RIGHT: begin
								if(iswall == 1'b1 && cube_x[0] == 38)
									hit_wall <= 1;
                                else begin
									if(cube_x[0] == 38) begin
								       cube_x[0] <= 1; 
								    end
								    else begin
									   cube_x[0] <= cube_x[0]+1;
									end
								end
							end
						endcase				//use direction to decide head's next position
						                    //meanwhile detect whether hit wall
					end
					if(player_num_state == 0) begin
					   if((other_head_y == cube_y[1] && other_head_x == cube_x[1] && is_exist[1] == 1)|
							(other_head_y == cube_y[2] && other_head_x == cube_x[2] && is_exist[2] == 1)|
							(other_head_y == cube_y[3] && other_head_x == cube_x[3] && is_exist[3] == 1)|
							(other_head_y == cube_y[4] && other_head_x == cube_x[4] && is_exist[4] == 1)|
							(other_head_y == cube_y[5] && other_head_x == cube_x[5] && is_exist[5] == 1)|
							(other_head_y == cube_y[6] && other_head_x == cube_x[6] && is_exist[6] == 1)|
							(other_head_y == cube_y[7] && other_head_x == cube_x[7] && is_exist[7] == 1)|
							(other_head_y == cube_y[8] && other_head_x == cube_x[8] && is_exist[8] == 1)|
							(other_head_y == cube_y[9] && other_head_x == cube_x[9] && is_exist[9] == 1)|
							(other_head_y == cube_y[10] && other_head_x == cube_x[10] && is_exist[10] == 1)|
							(other_head_y == cube_y[11] && other_head_x == cube_x[11] && is_exist[11] == 1)|
							(other_head_y == cube_y[12] && other_head_x == cube_x[12] && is_exist[12] == 1)|
							(other_head_y == cube_y[13] && other_head_x == cube_x[13] && is_exist[13] == 1)|
							(other_head_y == cube_y[14] && other_head_x == cube_x[14] && is_exist[14] == 1)|
							(other_head_y == cube_y[15] && other_head_x == cube_x[15] && is_exist[15] == 1))
							otherhit <= 1;//dectect head position with every body position, if it's same
							              //and this body exist, collision
				        else if((other_head_y == cube_y[16] && other_head_x == cube_x[16] && is_exist[16] == 1)|
							(other_head_y == cube_y[17] && other_head_x == cube_x[17] && is_exist[17] == 1)|
							(other_head_y == cube_y[18] && other_head_x == cube_x[18] && is_exist[18] == 1)|
							(other_head_y == cube_y[19] && other_head_x == cube_x[19] && is_exist[19] == 1)|
							(other_head_y == cube_y[20] && other_head_x == cube_x[20] && is_exist[20] == 1)|
							(other_head_y == cube_y[21] && other_head_x == cube_x[21] && is_exist[21] == 1)|
							(other_head_y == cube_y[22] && other_head_x == cube_x[22] && is_exist[22] == 1)|
							(other_head_y == cube_y[23] && other_head_x == cube_x[23] && is_exist[23] == 1)|
							(other_head_y == cube_y[24] && other_head_x == cube_x[24] && is_exist[24] == 1)|
							(other_head_y == cube_y[25] && other_head_x == cube_x[25] && is_exist[25] == 1)|
							(other_head_y == cube_y[26] && other_head_x == cube_x[26] && is_exist[26] == 1)|
							(other_head_y == cube_y[27] && other_head_x == cube_x[27] && is_exist[27] == 1)|
							(other_head_y == cube_y[28] && other_head_x == cube_x[28] && is_exist[28] == 1)|
							(other_head_y == cube_y[29] && other_head_x == cube_x[29] && is_exist[29] == 1)|
							(other_head_y == cube_y[30] && other_head_x == cube_x[30] && is_exist[30] == 1)|
							(other_head_y == cube_y[31] && other_head_x == cube_x[31] && is_exist[31] == 1)|
							(other_head_y == cube_y[32] && other_head_x == cube_x[32] && is_exist[32] == 1)|
							(other_head_y == cube_y[33] && other_head_x == cube_x[33] && is_exist[33] == 1)|
							(other_head_y == cube_y[34] && other_head_x == cube_x[34] && is_exist[34] == 1)|
							(other_head_y == cube_y[35] && other_head_x == cube_x[35] && is_exist[35] == 1))
							otherhit <= 1;
					
					    else if((other_head_y == cube_y[36] && other_head_x == cube_x[36] && is_exist[36] == 1)|
							(other_head_y == cube_y[37] && other_head_x == cube_x[37] && is_exist[37] == 1)|
							(other_head_y == cube_y[38] && other_head_x == cube_x[38] && is_exist[38] == 1)|
							(other_head_y == cube_y[39] && other_head_x == cube_x[39] && is_exist[39] == 1)|
							(other_head_y == cube_y[40] && other_head_x == cube_x[40] && is_exist[40] == 1)|
							(other_head_y == cube_y[41] && other_head_x == cube_x[41] && is_exist[41] == 1)|
							(other_head_y == cube_y[42] && other_head_x == cube_x[42] && is_exist[42] == 1)|
							(other_head_y == cube_y[43] && other_head_x == cube_x[43] && is_exist[43] == 1)|
							(other_head_y == cube_y[44] && other_head_x == cube_x[44] && is_exist[44] == 1)|
							(other_head_y == cube_y[45] && other_head_x == cube_x[45] && is_exist[45] == 1)|
							(other_head_y == cube_y[46] && other_head_x == cube_x[46] && is_exist[46] == 1)|
							(other_head_y == cube_y[47] && other_head_x == cube_x[47] && is_exist[47] == 1)|
							(other_head_y == cube_y[48] && other_head_x == cube_x[48] && is_exist[48] == 1)|
							(other_head_y == cube_y[49] && other_head_x == cube_x[49] && is_exist[49] == 1)|
							(other_head_y == cube_y[50] && other_head_x == cube_x[50] && is_exist[50] == 1))
							otherhit <= 1;
						else
						    otherhit <= 0;
					end
					else begin
					   otherhit <= 1'b0;
					end
				end
			end
		end
	end
	
	always @(*) begin   //push botton to decide the next direction
		direct_next = direct;		
        case(direct)	
		    UP: begin 
			    if(change_to_left)
				    direct_next = LEFT;
			    else if(change_to_right)
				    direct_next = RIGHT;
			    else
				    direct_next = UP;			
		    end		
			
		    DOWN: begin
			    if(change_to_left)
				    direct_next = LEFT;
			    else if(change_to_right)
			        direct_next = RIGHT;
			    else
				    direct_next = DOWN;			
		    end		
			
		    LEFT: begin
			    if(change_to_up)
				    direct_next = UP;
			    else if(change_to_down)
    			    direct_next = DOWN;
			    else
				    direct_next = LEFT;			
		    end
		
		    RIGHT: begin
			    if(change_to_up)
				    direct_next = UP;
			    else if(change_to_down)
				    direct_next = DOWN;
			    else
				    direct_next = RIGHT;
		    end	
	    endcase
	end
	
	always @(posedge clk) begin  // while press botton, set different value to 1
		if(left_press == 1)
			change_to_left <= 1;
		else if(right_press == 1)
			change_to_right <= 1;
		else if(up_press == 1)
			change_to_up <= 1;
		else if(down_press == 1)
			change_to_down <= 1;
		else begin
			change_to_left <= 0;
			change_to_right <= 0;
			change_to_up <= 0;
			change_to_down <= 0;
		end
	end
	
	//detect eating apple, while eating, add_cube =1,is_exist[k] = 1
	always @(posedge clk or negedge rst) begin 
		if(!rst) begin
			is_exist <= 51'd7; //16'b0000_0000_0000_0111
			cube_num <= 3;
			addcube_state <= 0;//start with length 3
		end  
		else if (game_status == RESTART) begin
		      is_exist <= 51'd7;
              cube_num <= 3;
              addcube_state <= 0;
         end
		else begin			
			case(addcube_state) //whether head and apple's position overlap
				0:begin
					if(add_cube) begin
						cube_num <= cube_num + 1;
						is_exist[cube_num] <= 1;
						addcube_state <= 1;//eating signal
					end						
				end
				1:begin
					if(!add_cube)
						addcube_state <= 0;				
				end
			endcase
		end
	end
	
	reg[3:0]lox;
	reg[3:0]loy;
	
	//snake is current object, unit:block
	always @(x_pos or y_pos ) begin				
		if(x_pos >= 0 && x_pos < 640 && y_pos >= 0 && y_pos < 480) begin
			if(x_pos[9:4] == 0 | y_pos[9:4] == 0 | x_pos[9:4] == 39 | y_pos[9:4] == 29) begin
				    snake = WALL;
			end
			else if(x_pos[9:4] == cube_x[0] && y_pos[9:4] == cube_y[0] && is_exist[0] == 1) 
				snake = (die_flash == 1) ? HEAD : NONE;
			else if
				((x_pos[9:4] == cube_x[1] && y_pos[9:4] == cube_y[1] && is_exist[1] == 1)|
				 (x_pos[9:4] == cube_x[2] && y_pos[9:4] == cube_y[2] && is_exist[2] == 1)|
				 (x_pos[9:4] == cube_x[3] && y_pos[9:4] == cube_y[3] && is_exist[3] == 1)|
				 (x_pos[9:4] == cube_x[4] && y_pos[9:4] == cube_y[4] && is_exist[4] == 1)|
				 (x_pos[9:4] == cube_x[5] && y_pos[9:4] == cube_y[5] && is_exist[5] == 1)|
				 (x_pos[9:4] == cube_x[6] && y_pos[9:4] == cube_y[6] && is_exist[6] == 1)|
				 (x_pos[9:4] == cube_x[7] && y_pos[9:4] == cube_y[7] && is_exist[7] == 1)|
				 (x_pos[9:4] == cube_x[8] && y_pos[9:4] == cube_y[8] && is_exist[8] == 1)|
				 (x_pos[9:4] == cube_x[9] && y_pos[9:4] == cube_y[9] && is_exist[9] == 1)|
				 (x_pos[9:4] == cube_x[10] && y_pos[9:4] == cube_y[10] && is_exist[10] == 1)|
				 (x_pos[9:4] == cube_x[11] && y_pos[9:4] == cube_y[11] && is_exist[11] == 1)|
				 (x_pos[9:4] == cube_x[12] && y_pos[9:4] == cube_y[12] && is_exist[12] == 1)|
				 (x_pos[9:4] == cube_x[13] && y_pos[9:4] == cube_y[13] && is_exist[13] == 1)|
				 (x_pos[9:4] == cube_x[14] && y_pos[9:4] == cube_y[14] && is_exist[14] == 1)|
				 (x_pos[9:4] == cube_x[15] && y_pos[9:4] == cube_y[15] && is_exist[15] == 1))
				 snake = (die_flash == 1) ? BODY : NONE;
			else if
				((x_pos[9:4] == cube_x[16] && y_pos[9:4] == cube_y[16] && is_exist[16] == 1)|
				 (x_pos[9:4] == cube_x[17] && y_pos[9:4] == cube_y[17] && is_exist[17] == 1)|
				 (x_pos[9:4] == cube_x[18] && y_pos[9:4] == cube_y[18] && is_exist[18] == 1)|
				 (x_pos[9:4] == cube_x[19] && y_pos[9:4] == cube_y[19] && is_exist[19] == 1)|
				 (x_pos[9:4] == cube_x[20] && y_pos[9:4] == cube_y[20] && is_exist[20] == 1)|
				 (x_pos[9:4] == cube_x[21] && y_pos[9:4] == cube_y[21] && is_exist[21] == 1)|
				 (x_pos[9:4] == cube_x[22] && y_pos[9:4] == cube_y[22] && is_exist[22] == 1)|
				 (x_pos[9:4] == cube_x[23] && y_pos[9:4] == cube_y[23] && is_exist[23] == 1)|
				 (x_pos[9:4] == cube_x[24] && y_pos[9:4] == cube_y[24] && is_exist[24] == 1)|
				 (x_pos[9:4] == cube_x[25] && y_pos[9:4] == cube_y[25] && is_exist[25] == 1)|
				 (x_pos[9:4] == cube_x[26] && y_pos[9:4] == cube_y[26] && is_exist[26] == 1)|
				 (x_pos[9:4] == cube_x[27] && y_pos[9:4] == cube_y[27] && is_exist[27] == 1)|
				 (x_pos[9:4] == cube_x[28] && y_pos[9:4] == cube_y[28] && is_exist[28] == 1)|
				 (x_pos[9:4] == cube_x[29] && y_pos[9:4] == cube_y[29] && is_exist[29] == 1)|
				 (x_pos[9:4] == cube_x[30] && y_pos[9:4] == cube_y[30] && is_exist[30] == 1)|
				 (x_pos[9:4] == cube_x[31] && y_pos[9:4] == cube_y[31] && is_exist[31] == 1)|
				 (x_pos[9:4] == cube_x[32] && y_pos[9:4] == cube_y[32] && is_exist[32] == 1)|
				 (x_pos[9:4] == cube_x[33] && y_pos[9:4] == cube_y[33] && is_exist[33] == 1)|
				 (x_pos[9:4] == cube_x[34] && y_pos[9:4] == cube_y[34] && is_exist[34] == 1)|
				 (x_pos[9:4] == cube_x[35] && y_pos[9:4] == cube_y[35] && is_exist[35] == 1))
				 snake = (die_flash == 1) ? BODY : NONE; 
            else if
				((x_pos[9:4] == cube_x[36] && y_pos[9:4] == cube_y[36] && is_exist[36] == 1)|
				 (x_pos[9:4] == cube_x[37] && y_pos[9:4] == cube_y[37] && is_exist[37] == 1)|
				 (x_pos[9:4] == cube_x[38] && y_pos[9:4] == cube_y[38] && is_exist[38] == 1)|
				 (x_pos[9:4] == cube_x[39] && y_pos[9:4] == cube_y[39] && is_exist[39] == 1)|
				 (x_pos[9:4] == cube_x[40] && y_pos[9:4] == cube_y[40] && is_exist[40] == 1)|
				 (x_pos[9:4] == cube_x[41] && y_pos[9:4] == cube_y[41] && is_exist[41] == 1)|
				 (x_pos[9:4] == cube_x[42] && y_pos[9:4] == cube_y[42] && is_exist[42] == 1)|
				 (x_pos[9:4] == cube_x[43] && y_pos[9:4] == cube_y[43] && is_exist[43] == 1)|
				 (x_pos[9:4] == cube_x[44] && y_pos[9:4] == cube_y[44] && is_exist[44] == 1)|
				 (x_pos[9:4] == cube_x[45] && y_pos[9:4] == cube_y[45] && is_exist[45] == 1)|
				 (x_pos[9:4] == cube_x[46] && y_pos[9:4] == cube_y[46] && is_exist[46] == 1)|
				 (x_pos[9:4] == cube_x[47] && y_pos[9:4] == cube_y[47] && is_exist[47] == 1)|
				 (x_pos[9:4] == cube_x[48] && y_pos[9:4] == cube_y[48] && is_exist[48] == 1)|
				 (x_pos[9:4] == cube_x[49] && y_pos[9:4] == cube_y[49] && is_exist[49] == 1)|
				 (x_pos[9:4] == cube_x[50] && y_pos[9:4] == cube_y[50] && is_exist[50] == 1))
				 snake = (die_flash == 1) ? BODY : NONE; 
			else snake = NONE;
		end
	end
endmodule