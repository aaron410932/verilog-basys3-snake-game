module music_control (
	input clk,
	input reset,
	input [1:0] game_status,
	output reg [11:0] ibeat,
	output diemusic
);
	parameter LEN = 4095;
    reg [11:0] next_ibeat;
    
    reg [11:0] tempibeat;
    reg die = 0;
    assign diemusic = die;
    always @(posedge clk) begin
        if(reset) begin
            ibeat <= 0;
            die <= 0;
        end
        if(game_status == 2'b11 && die == 0) begin
            ibeat <= 0;
            die <= 1;
            tempibeat <= ibeat;
        end
        else if(die == 1) begin
            if(ibeat + 1 < 32) begin
                ibeat <= ibeat + 1;
            end
            else begin
                ibeat <= tempibeat;
                die <= 0;
            end
        end
        else if(ibeat + 1 < LEN) begin
            die <= 0;
            ibeat <= ibeat + 1;
        end
        else begin
            ibeat <= 12'd0;
        end
    end

endmodule