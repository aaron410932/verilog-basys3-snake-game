module Transition(
	output reg left,
	output reg right,
	output reg up,
	output reg down,
	output reg w1,
	output reg a1,
	output reg s1,
	output reg d1,
	inout wire PS2_DATA,
	inout wire PS2_CLK,
	input wire rst,
	input wire clk
	);
	
	parameter [8:0] LEFT_SHIFT_CODES  = 9'b0_0001_0010;
	parameter [8:0] RIGHT_SHIFT_CODES = 9'b0_0101_1001;
	parameter [8:0] KEY_CODES [0:7] = {
		/*9'b0_0100_0101,	// 0 => 45
		9'b0_0001_0110,	// 1 => 16
		9'b0_0001_1110,	// 2 => 1E
		9'b0_0010_0110,	// 3 => 26
		9'b0_0010_0101,	// 4 => 25
		9'b0_0010_1110,	// 5 => 2E
		9'b0_0011_0110,	// 6 => 36
		9'b0_0011_1101,	// 7 => 3D
		9'b0_0011_1110,	// 8 => 3E
		9'b0_0100_0110,	// 9 => 46
		
		9'b0_0111_0000, // right_0 => 70
		9'b0_0110_1001, // right_1 => 69
		9'b0_0111_0010, // right_2 => 72
		9'b0_0111_1010, // right_3 => 7A
		9'b0_0110_1011, // right_4 => 6B
		9'b0_0111_0011, // right_5 => 73
		9'b0_0111_0100, // right_6 => 74
		9'b0_0110_1100, // right_7 => 6C
		9'b0_0111_0101, // right_8 => 75
		9'b0_0111_1101  // right_9 => 7D*/
		9'b0_0110_1011, //left
		9'b0_0111_0101, //up
		9'b1_0111_0100, //right
		9'b0_0111_0010, //down
		9'b0_0001_1101, //W
		9'b0_0001_1100, //A
		9'b0_0001_1011, //S
		9'b0_0010_0011  //D
	};
	
	reg [15:0] nums;

	reg [9:0] last_key;
	
	wire shift_down;
	wire [511:0] key_down;
	wire [8:0] last_change;
	wire been_ready;
	
	assign shift_down = (key_down[LEFT_SHIFT_CODES] == 1'b1 || key_down[RIGHT_SHIFT_CODES] == 1'b1) ? 1'b1 : 1'b0;
		
	KeyboardDecoder key_de (
		.key_down(key_down),
		.last_change(last_change),
		.key_valid(been_ready),
		.PS2_DATA(PS2_DATA),
		.PS2_CLK(PS2_CLK),
		.rst(rst),
		.clk(clk)
	);

	/*always @ (posedge clk, posedge rst) begin
		if (rst) begin
			nums <= 16'b0;
		end else begin
			nums <= nums;
			if (been_ready && key_down[last_change] == 1'b1) begin
				if (key_num != 4'b1111)begin
					if (shift_down == 1'b1) begin
						nums <= {key_num, nums[15:4]};
					end else begin
						nums <= {nums[11:0], key_num};
					end
				end
			end
		end
	end*/
	reg [3:0] key_num = 4'b0;
	//assign left = ()
	always @ (*) begin
		case (last_change)
			KEY_CODES[0] : begin
			    left = 1;
			    up = 0;
			    right = 0;
			    down = 0;
			    w1 = 0;
			    a1 = 0;
			    s1 = 0;
			    d1 = 0;
			end
			KEY_CODES[1] : begin
			    left = 0;
			    up = 1;
			    right = 0;
			    down = 0;
			    w1 = 0;
			    a1 = 0;
			    s1 = 0;
			    d1 = 0;
			end
			KEY_CODES[2] : begin
			    left = 0;
			    up = 0;
			    right = 1;
			    down = 0;
			    w1 = 0;
			    a1 = 0;
			    s1 = 0;
			    d1 = 0;
			end
			KEY_CODES[3] : begin
			    left = 0;
			    up = 0;
			    right = 0;
			    down = 1;
			    w1 = 0;
			    a1 = 0;
			    s1 = 0;
			    d1 = 0;
			end
			KEY_CODES[4] : begin
			    w1 = 1;
			    a1 = 0;
			    s1 = 0;
			    d1 = 0;
			    left = 0;
			    up = 0;
			    right = 0;
			    down = 0;
			end
			KEY_CODES[5] : begin
			    w1 = 0;
			    a1 = 1;
			    s1 = 0;
			    d1 = 0;
			    left = 0;
			    up = 0;
			    right = 0;
			    down = 0;
			end
			KEY_CODES[6] : begin
			    w1 = 0;
			    a1 = 0;
			    s1 = 1;
			    d1 = 0;
			    left = 0;
			    up = 0;
			    right = 0;
			    down = 0;
			end
			KEY_CODES[7] : begin
			    w1 = 0;
			    a1 = 0;
			    s1 = 0;
			    d1 = 1;
			    left = 0;
			    up = 0;
			    right = 0;
			    down = 0;
			end
			/*KEY_CODES[04] : key_num = 4'b0100;
			KEY_CODES[05] : key_num = 4'b0101;
			KEY_CODES[06] : key_num = 4'b0110;
			KEY_CODES[07] : key_num = 4'b0111;
			KEY_CODES[08] : key_num = 4'b1000;
			KEY_CODES[09] : key_num = 4'b1001;
			KEY_CODES[10] : key_num = 4'b0000;
			KEY_CODES[11] : key_num = 4'b0001;
			KEY_CODES[12] : key_num = 4'b0010;
			KEY_CODES[13] : key_num = 4'b0011;
			KEY_CODES[14] : key_num = 4'b0100;
			KEY_CODES[15] : key_num = 4'b0101;
			KEY_CODES[16] : key_num = 4'b0110;
			KEY_CODES[17] : key_num = 4'b0111;
			KEY_CODES[18] : key_num = 4'b1000;
			KEY_CODES[19] : key_num = 4'b1001;
			default		  : key_num = 4'b1111;*/
		endcase
	end
	
endmodule
