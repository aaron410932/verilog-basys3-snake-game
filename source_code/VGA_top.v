`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/01/23 23:55:53
// Design Name: 
// Module Name: VGA_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_top(
	input clk,
    input rst,
    input start,
    input [1:0] game_status,
    input hsync_i,
    input vsync_i,
    input [9:0] h_cnt,
    input [9:0] v_cnt,
    input wallmode,
    input player_num_state,
    input [11:0] pixel,
    input special,
    input [2:0]snake,
    input [2:0]snake_p2,
    input [5:0]apple_x,
    input [4:0]apple_y,
    output [9:0]x_pos,
    output [9:0]y_pos,    
    output hsync,
    output vsync,
    output [11:0] color_out
    );
    
    wire clk_n;
    
    clk_unit myclk(
        .clk(clk),
        .rst(rst),
        .clk_n(clk_n)
    );


    VGA_Control VGA
    (
		.clk(clk_n),
		.rst(rst),
		.start(start),
		.game_status(game_status),
		.special(special),
		.hsync_i(hsync_i),
		.vsync_i(vsync_i),
		.pixel(pixel),
		.hsync(hsync),
		.vsync(vsync),
		.h_cnt(h_cnt),
		.v_cnt(v_cnt),
		.wallmode(wallmode),
		.player_num_state(player_num_state),
		.snake(snake),
		.snake_p2(snake_p2),
        .color_out(color_out),
		.x_pos(x_pos),
		.y_pos(y_pos),
		.apple_x(apple_x),
		.apple_y(apple_y)
	);
endmodule
