`define c3 32'd131 
`define d3 32'd147
`define e3 32'd165
`define f3 32'd174
`define g3 32'd196
`define a3 32'd220
`define sa3 32'd233 //a#3
`define b3 32'd247
`define c4 32'd262
`define sc4 32'd277 //c#4
`define d4 32'd294
`define e4 32'd330
`define f4 32'd349
`define sf4 32'd370
`define g4 32'd392
`define a4 32'd440
`define bb4 32'd466
`define b4 32'd494
`define c5 32'd524
`define d5 32'd588
`define e5 32'd660
`define f5 32'd698
`define g5 32'd784
`define a5 32'd880
`define b5 32'd988 
`define sil   32'd50000000 // slience

module music_example (
    input [11:0] ibeatNum,
    input en,
    output reg [31:0] toneL,
    output reg [31:0] toneR
);
 
    always @* begin
        if(en == 0) begin
            case(ibeatNum)
                //Measure 1
                12'd0: toneR = `e5;     12'd1: toneR = `e5;
                12'd2: toneR = `e5;     12'd3: toneR = `e5;
                12'd4: toneR = `e5;     12'd5: toneR = `e5;
                12'd6: toneR = `e5;     12'd7: toneR = `sil;
                12'd8: toneR = `e5;     12'd9: toneR = `e5;
                12'd10: toneR = `e5;    12'd11: toneR = `e5;
                12'd12: toneR = `e5;    12'd13: toneR = `e5;
                12'd14: toneR = `e5;    12'd15: toneR = `sil;
 
                12'd16: toneR = `sil;    12'd17: toneR = `sil;
                12'd18: toneR = `sil;    12'd19: toneR = `sil;
                12'd20: toneR = `sil;    12'd21: toneR = `sil;
                12'd22: toneR = `sil;    12'd23: toneR = `sil;
                12'd24: toneR = `e5;    12'd25: toneR = `e5;
                12'd26: toneR = `e5;    12'd27: toneR = `e5;
                12'd28: toneR = `e5;    12'd29: toneR = `e5;
                12'd30: toneR = `e5;    12'd31: toneR = `sil;
 
                12'd32: toneR = `sil;    12'd33: toneR = `sil;
                12'd34: toneR = `sil;    12'd35: toneR = `sil;
                12'd36: toneR = `sil;    12'd37: toneR = `sil;
                12'd38: toneR = `sil;    12'd39: toneR = `sil;
                12'd40: toneR = `c5;    12'd41: toneR = `c5;
                12'd42: toneR = `c5;    12'd43: toneR = `c5;
                12'd44: toneR = `c5;    12'd45: toneR = `c5;
                12'd46: toneR = `c5;    12'd47: toneR = `sil;
 
                12'd48: toneR = `e5;    12'd49: toneR = `e5;
                12'd50: toneR = `e5;    12'd51: toneR = `e5;
                12'd52: toneR = `e5;    12'd53: toneR = `e5;
                12'd54: toneR = `e5;    12'd55: toneR = `e5;
                12'd56: toneR = `e5;   12'd57: toneR = `e5;
                12'd58: toneR = `e5;   12'd59: toneR = `e5;
                12'd60: toneR = `e5;   12'd61: toneR = `e5;
                12'd62: toneR = `e5;   12'd63: toneR = `sil;
                //Measure 2
                12'd64: toneR = `g5;    12'd65: toneR = `g5;
                12'd66: toneR = `g5;    12'd67: toneR = `g5;
                12'd68: toneR = `g5;    12'd69: toneR = `g5;
                12'd70: toneR = `g5;    12'd71: toneR = `g5;
                12'd72: toneR = `g5;    12'd73: toneR = `g5;
                12'd74: toneR = `g5;    12'd75: toneR = `g5;
                12'd76: toneR = `g5;    12'd77: toneR = `g5;
                12'd78: toneR = `g5;    12'd79: toneR = `sil;
 
                12'd80: toneR = `sil;    12'd81: toneR = `sil;
                12'd82: toneR = `sil;    12'd83: toneR = `sil;
                12'd84: toneR = `sil;    12'd85: toneR = `sil;
                12'd86: toneR = `sil;    12'd87: toneR = `sil;
                12'd88: toneR = `sil;    12'd89: toneR = `sil;
                12'd90: toneR = `sil;    12'd91: toneR = `sil;
                12'd92: toneR = `sil;    12'd93: toneR = `sil;
                12'd94: toneR = `sil;    12'd95: toneR = `sil;
 
                12'd96: toneR = `g4;    12'd97: toneR = `g4;
                12'd98: toneR = `g4;    12'd99: toneR = `g4;
                12'd100: toneR = `g4;   12'd101: toneR = `g4;
                12'd102: toneR = `g4;   12'd103: toneR = `g4;
                12'd104: toneR = `g4;   12'd105: toneR = `g4;
                12'd106: toneR = `g4;   12'd107: toneR = `g4;
                12'd108: toneR = `g4;   12'd109: toneR = `g4;
                12'd110: toneR = `g4;   12'd111: toneR = `sil;
 
                12'd112: toneR = `sil;   12'd113: toneR = `sil;
                12'd114: toneR = `sil;   12'd115: toneR = `sil;
                12'd116: toneR = `sil;   12'd117: toneR = `sil;
                12'd118: toneR = `sil;   12'd119: toneR = `sil;
                12'd120: toneR = `sil;  12'd121: toneR = `sil;
                12'd122: toneR = `sil;  12'd123: toneR = `sil;
                12'd124: toneR = `sil;  12'd125: toneR = `sil;
                12'd126: toneR = `sil;  12'd127: toneR = `sil;
                //Measure 3
                12'd128: toneR = `c5;   12'd129: toneR = `c5;
                12'd130: toneR = `c5;   12'd131: toneR = `c5;
                12'd132: toneR = `c5;   12'd133: toneR = `c5;
                12'd134: toneR = `c5;   12'd135: toneR = `c5;
                12'd136: toneR = `c5;   12'd137: toneR = `c5;
                12'd138: toneR = `c5;   12'd139: toneR = `c5;
                12'd140: toneR = `c5;   12'd141: toneR = `c5;
                12'd142: toneR = `c5;   12'd143: toneR = `sil;
 
                12'd144: toneR = `sil;  12'd145: toneR = `sil;
                12'd146: toneR = `sil;  12'd147: toneR = `sil;
                12'd148: toneR = `sil;  12'd149: toneR = `sil;
                12'd150: toneR = `sil;  12'd151: toneR = `sil;
                12'd152: toneR = `g4;   12'd153: toneR = `g4;
                12'd154: toneR = `g4;   12'd155: toneR = `g4;
                12'd156: toneR = `g4;   12'd157: toneR = `g4;
                12'd158: toneR = `g4;   12'd159: toneR = `g4;
 
                12'd160: toneR = `g4;  12'd161: toneR = `g4;
                12'd162: toneR = `g4;   12'd163: toneR = `g4;
                12'd164: toneR = `g4;   12'd165: toneR = `g4;
                12'd166: toneR = `g4;   12'd167: toneR = `sil;
                12'd168: toneR = `sil; 12'd169: toneR = `sil;
                12'd170: toneR = `sil;  12'd171: toneR = `sil;
                12'd172: toneR = `sil;  12'd173: toneR = `sil;
                12'd174: toneR = `sil; 12'd175: toneR = `sil;
 
                12'd176: toneR = `e4;   12'd177: toneR = `e4;
                12'd178: toneR = `e4;   12'd179: toneR = `e4;
                12'd180: toneR = `e4;   12'd181: toneR = `e4;
                12'd182: toneR = `e4;   12'd183: toneR = `e4;
                12'd184: toneR = `e4;   12'd185: toneR = `e4;
                12'd186: toneR = `e4;   12'd187: toneR = `e4;
                12'd188: toneR = `e4;   12'd189: toneR = `e4;
                12'd190: toneR = `e4;   12'd191: toneR = `sil;
                //Measure 4
                12'd192: toneR = `sil;  12'd193: toneR = `sil;
                12'd194: toneR = `sil;  12'd195: toneR = `sil;
                12'd196: toneR = `sil;  12'd197: toneR = `sil;
                12'd198: toneR = `sil;  12'd199: toneR = `sil;
                12'd200: toneR = `a4;   12'd201: toneR = `a4;
                12'd202: toneR = `a4;   12'd203: toneR = `a4;
                12'd204: toneR = `a4;   12'd205: toneR = `a4;
                12'd206: toneR = `a4;   12'd207: toneR = `a4;
 
                12'd208: toneR = `a4;   12'd209: toneR = `a4;
                12'd210: toneR = `a4;   12'd211: toneR = `a4;
                12'd212: toneR = `a4;   12'd213: toneR = `a4;
                12'd214: toneR = `a4;   12'd215: toneR = `sil;
                12'd216: toneR = `b4;   12'd217: toneR = `b4;
                12'd218: toneR = `b4;   12'd219: toneR = `b4;
                12'd220: toneR = `b4;   12'd221: toneR = `b4;
                12'd222: toneR = `b4;   12'd223: toneR = `b4;
 
                12'd224: toneR = `b4;   12'd225: toneR = `b4;
                12'd226: toneR = `b4;   12'd227: toneR = `b4;
                12'd228: toneR = `b4;   12'd229: toneR = `b4;
                12'd230: toneR = `b4;   12'd231: toneR = `sil;
                12'd232: toneR = `bb4;  12'd233: toneR = `bb4;
                12'd234: toneR = `bb4;   12'd235: toneR = `bb4;
                12'd236: toneR = `bb4;   12'd237: toneR = `bb4;
                12'd238: toneR = `bb4;   12'd239: toneR = `sil;
 
                12'd240: toneR = `a4;   12'd241: toneR = `a4;
                12'd242: toneR = `a4;   12'd243: toneR = `a4;
                12'd244: toneR = `a4;   12'd245: toneR = `a4;
                12'd246: toneR = `a4;   12'd247: toneR = `a4;
                12'd248: toneR = `a4;   12'd249: toneR = `a4;
                12'd250: toneR = `a4;   12'd251: toneR = `a4;
                12'd252: toneR = `a4;   12'd253: toneR = `a4;
                12'd254: toneR = `a4;   12'd255: toneR = `a4;
                //Measure 5
                12'd256: toneR = `g4;   12'd257: toneR = `g4;
                12'd258: toneR = `g4;   12'd259: toneR = `g4;
                12'd260: toneR = `g4;   12'd261: toneR = `g4;
                12'd262: toneR = `g4;    12'd263: toneR = `g4;
                12'd264: toneR = `g4;   12'd265: toneR = `sil;
                12'd266: toneR = `e5;   12'd267: toneR = `e5;
                12'd268: toneR = `e5;   12'd269: toneR = `e5;
                12'd270: toneR = `e5;   12'd271: toneR = `e5;
 
                12'd272: toneR = `e5;   12'd273: toneR = `e5;
                12'd274: toneR = `e5;   12'd275: toneR = `e5;
                12'd276: toneR = `sil;  12'd277: toneR = `g5;
                12'd278: toneR = `g5;   12'd279: toneR = `g5;
                12'd280: toneR = `g5;   12'd281: toneR = `g5;
                12'd282: toneR = `g5;   12'd283: toneR = `g5;
                12'd284: toneR = `g5;   12'd285: toneR = `g5;
                12'd286: toneR = `g5;   12'd287: toneR = `sil;
 
                12'd288: toneR = `a5;   12'd289: toneR = `a5;
                12'd290: toneR = `a5;   12'd291: toneR = `a5;
                12'd292: toneR = `a5;   12'd293: toneR = `a5;
                12'd294: toneR = `a5;   12'd295: toneR = `a5;
                12'd296: toneR = `a5;   12'd297: toneR = `a5;
                12'd298: toneR = `a5;   12'd299: toneR = `a5;
                12'd300: toneR = `a5;   12'd301: toneR = `a5;
                12'd302: toneR = `a5;   12'd303: toneR = `sil;
 
                12'd304: toneR = `f5;   12'd305: toneR = `f5;
                12'd306: toneR = `f5;   12'd307: toneR = `f5;
                12'd308: toneR = `f5;   12'd309: toneR = `f5;
                12'd310: toneR = `f5;   12'd311: toneR = `sil;
                12'd312: toneR = `g5;   12'd313: toneR = `g5;
                12'd314: toneR = `g5;   12'd315: toneR = `g5;
                12'd316: toneR = `g5;   12'd317: toneR = `g5;
                12'd318: toneR = `g5;   12'd319: toneR = `sil;
                //Measure 6
                12'd320: toneR = `sil;   12'd321: toneR = `sil;
                12'd322: toneR = `sil;   12'd323: toneR = `sil;
                12'd324: toneR = `sil;   12'd325: toneR = `sil;
                12'd326: toneR = `sil;   12'd327: toneR = `sil;
                12'd328: toneR = `e5;   12'd329: toneR = `e5;
                12'd330: toneR = `e5;   12'd331: toneR = `e5;
                12'd332: toneR = `e5;   12'd333: toneR = `e5;
                12'd334: toneR = `e5;   12'd335: toneR = `e5;
 
                12'd336: toneR = `e5;   12'd337: toneR = `e5;
                12'd338: toneR = `e5;   12'd339: toneR = `e5;
                12'd340: toneR = `e5;   12'd341: toneR = `e5;
                12'd342: toneR = `e5;   12'd343: toneR = `sil;
                12'd344: toneR = `c5;   12'd345: toneR = `c5;
                12'd346: toneR = `c5;   12'd347: toneR = `c5;
                12'd348: toneR = `c5;   12'd349: toneR = `c5;
                12'd350: toneR = `c5;   12'd351: toneR = `sil;
 
                12'd352: toneR = `d5;   12'd353: toneR = `d5;
                12'd354: toneR = `d5;   12'd355: toneR = `d5;
                12'd356: toneR = `d5;   12'd357: toneR = `d5;
                12'd358: toneR = `d5;   12'd359: toneR = `sil;
                12'd360: toneR = `b4;   12'd361: toneR = `b4;
                12'd362: toneR = `b4;   12'd363: toneR = `b4;
                12'd364: toneR = `b4;   12'd365: toneR = `b4;
                12'd366: toneR = `b4;   12'd367: toneR = `b4;
 
                12'd368: toneR = `b4;   12'd369: toneR = `b4;
                12'd370: toneR = `b4;   12'd371: toneR = `b4;
                12'd372: toneR = `b4;   12'd373: toneR = `b4;
                12'd374: toneR = `b4;   12'd375: toneR = `sil;
                12'd376: toneR = `sil;   12'd377: toneR = `sil;
                12'd378: toneR = `sil;   12'd379: toneR = `sil;
                12'd380: toneR = `sil;   12'd381: toneR = `sil;
                12'd382: toneR = `sil;   12'd383: toneR = `sil;
                default: toneR = `sil;
            endcase
        end        
        if(en == 1) begin/*gmae over music*/
        case(ibeatNum)
                //Measure 1
                12'd0: toneR = `d5;     12'd1: toneR = `d5;
                12'd2: toneR = `d5;     12'd3: toneR = `d5;
                12'd4: toneR = `d5;     12'd5: toneR = `d5;
                12'd6: toneR = `d5;     12'd7: toneR = `d5;
                12'd8: toneR = `a4;     12'd9: toneR = `a4;
                12'd10: toneR = `a4;    12'd11: toneR = `a4;
                12'd12: toneR = `a4;    12'd13: toneR = `a4;
                12'd14: toneR = `a4;    12'd15: toneR = `a4;
 
                12'd16: toneR = `sf4;    12'd17: toneR = `sf4;
                12'd18: toneR = `sf4;    12'd19: toneR = `sf4;
                12'd20: toneR = `sf4;    12'd21: toneR = `sf4;
                12'd22: toneR = `sf4;    12'd23: toneR = `sf4;
                12'd24: toneR = `d4;    12'd25: toneR = `d4;
                12'd26: toneR = `d4;    12'd27: toneR = `d4;
                12'd28: toneR = `d4;    12'd29: toneR = `d4;
                12'd30: toneR = `d4;    12'd31: toneR = `d4;
                default : toneR = `sil;
			endcase
		end
    end
 
    always @(*) begin
        if(en==0)begin
            case(ibeatNum)
                //Measure 1
                12'd0: toneL = `sf4;     12'd1: toneL = `sf4;
                12'd2: toneL = `sf4;     12'd3: toneL = `sf4;
                12'd4: toneL = `sf4;     12'd5: toneL = `sf4;
                12'd6: toneL = `sf4;     12'd7: toneL = `sil;
                12'd8: toneL = `sf4;     12'd9: toneL = `sf4;
                12'd10: toneL = `sf4;    12'd11: toneL = `sf4;
                12'd12: toneL = `sf4;    12'd13: toneL = `sf4;
                12'd14: toneL = `sf4;    12'd15: toneL = `sil;
 
                12'd16: toneL = `sil;    12'd17: toneL = `sil;
                12'd18: toneL = `sil;    12'd19: toneL = `sil;
                12'd20: toneL = `sil;    12'd21: toneL = `sil;
                12'd22: toneL = `sil;    12'd23: toneL = `sil;
                12'd24: toneL = `sf4;    12'd25: toneL = `sf4;
                12'd26: toneL = `sf4;    12'd27: toneL = `sf4;
                12'd28: toneL = `sf4;    12'd29: toneL = `sf4;
                12'd30: toneL = `sf4;    12'd31: toneL = `sil;
 
                12'd32: toneL = `sil;     12'd33: toneL = `sil;
                12'd34: toneL = `sil;     12'd35: toneL = `sil;
                12'd36: toneL = `sil;     12'd37: toneL = `sil;
                12'd38: toneL = `sil;     12'd39: toneL = `sil;
                12'd40: toneL = `sf4;     12'd41: toneL = `sf4;
                12'd42: toneL = `sf4;     12'd43: toneL = `sf4;
                12'd44: toneL = `sf4;     12'd45: toneL = `sf4;
                12'd46: toneL = `sf4;     12'd47: toneL = `sf4;
 
                12'd48: toneL = `sf4;     12'd49: toneL = `sf4;
                12'd50: toneL = `sf4;     12'd51: toneL = `sf4;
                12'd52: toneL = `sf4;     12'd53: toneL = `sf4;
                12'd54: toneL = `sf4;     12'd55: toneL = `sf4;
                12'd56: toneL = `sf4;     12'd57: toneL = `sf4;
                12'd58: toneL = `sf4;     12'd59: toneL = `sf4;
                12'd60: toneL = `sf4;     12'd61: toneL = `sf4;
                12'd62: toneL = `sf4;     12'd63: toneL = `sil;
                //Measure 2
                12'd64: toneL = `b4;    12'd65: toneL = `b4;
                12'd66: toneL = `b4;    12'd67: toneL = `b4;
                12'd68: toneL = `b4;    12'd69: toneL = `b4;
                12'd70: toneL = `b4;    12'd71: toneL = `b4;
                12'd72: toneL = `b4;    12'd73: toneL = `b4;
                12'd74: toneL = `b4;    12'd75: toneL = `b4;
                12'd76: toneL = `b4;    12'd77: toneL = `b4;
                12'd78: toneL = `b4;    12'd79: toneL = `sil;
 
                12'd80: toneL = `sil;    12'd81: toneL = `sil;
                12'd82: toneL = `sil;    12'd83: toneL = `sil;
                12'd84: toneL = `sil;    12'd85: toneL = `sil;
                12'd86: toneL = `sil;    12'd87: toneL = `sil;
                12'd88: toneL = `sil;    12'd89: toneL = `sil;
                12'd90: toneL = `sil;    12'd91: toneL = `sil;
                12'd92: toneL = `sil;    12'd93: toneL = `sil;
                12'd94: toneL = `sil;    12'd95: toneL = `sil;
 
                12'd96: toneL = `g4;     12'd97: toneL = `g4;
                12'd98: toneL = `g4;     12'd99: toneL = `g4;
                12'd100: toneL = `g4;    12'd101: toneL = `g4;
                12'd102: toneL = `g4;    12'd103: toneL = `g4;
                12'd104: toneL = `g4;    12'd105: toneL = `g4;
                12'd106: toneL = `g4;    12'd107: toneL = `g4;
                12'd108: toneL = `g4;    12'd109: toneL = `g4;
                12'd110: toneL = `g4;    12'd111: toneL = `sil;
 
                12'd112: toneL = `sil;    12'd113: toneL = `sil;
                12'd114: toneL = `sil;    12'd115: toneL = `sil;
                12'd116: toneL = `sil;    12'd117: toneL = `sil;
                12'd118: toneL = `sil;    12'd119: toneL = `sil;
                12'd120: toneL = `sil;    12'd121: toneL = `sil;
                12'd122: toneL = `sil;    12'd123: toneL = `sil;
                12'd124: toneL = `sil;    12'd125: toneL = `sil;
                12'd126: toneL = `sil;    12'd127: toneL = `sil;
                //Measure 3
                12'd128: toneL = `e4;   12'd129: toneL = `e4;
                12'd130: toneL = `e4;   12'd131: toneL = `e4;
                12'd132: toneL = `e4;   12'd133: toneL = `e4;
                12'd134: toneL = `e4;   12'd135: toneL = `e4;
                12'd136: toneL = `e4;   12'd137: toneL = `e4;
                12'd138: toneL = `e4;   12'd139: toneL = `e4;
                12'd140: toneL = `e4;   12'd141: toneL = `e4;
                12'd142: toneL = `e4;   12'd143: toneL = `sil;
 
                12'd144: toneL = `sil;   12'd145: toneL = `sil;
                12'd146: toneL = `sil;   12'd147: toneL = `sil;
                12'd148: toneL = `sil;   12'd149: toneL = `sil;
                12'd150: toneL = `sil;   12'd151: toneL = `sil;
                12'd152: toneL = `c4;   12'd153: toneL = `c4;
                12'd154: toneL = `c4;   12'd155: toneL = `c4;
                12'd156: toneL = `c4;   12'd157: toneL = `c4;
                12'd158: toneL = `c4;   12'd159: toneL = `c4;
 
                12'd160: toneL = `c4;    12'd161: toneL = `c4;
                12'd162: toneL = `c4;    12'd163: toneL = `c4;
                12'd164: toneL = `c4;    12'd165: toneL = `c4;
                12'd166: toneL = `c4;    12'd167: toneL = `sil;
                12'd168: toneL = `sil;    12'd169: toneL = `sil;
                12'd170: toneL = `sil;    12'd171: toneL = `sil;
                12'd172: toneL = `sil;    12'd173: toneL = `sil;
                12'd174: toneL = `sil;    12'd175: toneL = `sil;
 
                12'd176: toneL = `g3;    12'd177: toneL = `g3;
                12'd178: toneL = `g3;    12'd179: toneL = `g3;
                12'd180: toneL = `g3;    12'd181: toneL = `g3;
                12'd182: toneL = `g3;    12'd183: toneL = `g3;
                12'd184: toneL = `g3;    12'd185: toneL = `g3;
                12'd186: toneL = `g3;    12'd187: toneL = `g3;
                12'd188: toneL = `g3;    12'd189: toneL = `g3;
                12'd190: toneL = `g3;    12'd191: toneL = `sil;
                //Measure 4
                12'd192: toneL = `sil;   12'd193: toneL = `sil;
                12'd194: toneL = `sil;   12'd195: toneL = `sil;
                12'd196: toneL = `sil;   12'd197: toneL = `sil;
                12'd198: toneL = `sil;   12'd199: toneL = `sil;
                12'd200: toneL = `c4;   12'd201: toneL = `c4;
                12'd202: toneL = `c4;   12'd203: toneL = `c4;
                12'd204: toneL = `c4;   12'd205: toneL = `c4;
                12'd206: toneL = `c4;   12'd207: toneL = `c4;
 
                12'd208: toneL = `c4;    12'd209: toneL = `c4;
                12'd210: toneL = `c4;    12'd211: toneL = `c4;
                12'd212: toneL = `c4;    12'd213: toneL = `c4;
                12'd214: toneL = `c4;    12'd215: toneL = `sil;
                12'd216: toneL = `d4;    12'd217: toneL = `d4;
                12'd218: toneL = `d4;    12'd219: toneL = `d4;
                12'd220: toneL = `d4;    12'd221: toneL = `d4;
                12'd222: toneL = `d4;    12'd223: toneL = `d4;
 
                12'd224: toneL = `d4;    12'd225: toneL = `d4;
                12'd226: toneL = `d4;    12'd227: toneL = `d4;
                12'd228: toneL = `d4;    12'd229: toneL = `d4;
                12'd230: toneL = `d4;    12'd231: toneL = `sil;
                12'd232: toneL = `sc4;    12'd233: toneL = `sc4;
                12'd234: toneL = `sc4;    12'd235: toneL = `sc4;
                12'd236: toneL = `sc4;    12'd237: toneL = `sc4;
                12'd238: toneL = `sc4;    12'd239: toneL = `sil;
 
                12'd240: toneL = `c4;    12'd241: toneL = `c4;
                12'd242: toneL = `c4;    12'd243: toneL = `c4;
                12'd244: toneL = `c4;    12'd245: toneL = `c4;
                12'd246: toneL = `c4;    12'd247: toneL = `c4;
                12'd248: toneL = `c4;    12'd249: toneL = `c4;
                12'd250: toneL = `c4;    12'd251: toneL = `c4;
                12'd252: toneL = `c4;    12'd253: toneL = `c4;
                12'd254: toneL = `c4;    12'd255: toneL = `sil;
                //Measure 5
                12'd256: toneL = `c4;    12'd257: toneL = `c4;
                12'd258: toneL = `c4;    12'd259: toneL = `c4;
                12'd260: toneL = `c4;    12'd261: toneL = `c4;
                12'd262: toneL = `c4;    12'd263: toneL = `c4;
                12'd264: toneL = `c4;    12'd265: toneL = `c4;
                12'd266: toneL = `sil;    12'd267: toneL = `g4;
                12'd268: toneL = `g4;    12'd269: toneL = `g4;
                12'd270: toneL = `g4;    12'd271: toneL = `g4;
 
                12'd272: toneL = `g4;    12'd273: toneL = `g4;
                12'd274: toneL = `g4;    12'd275: toneL = `g4;
                12'd276: toneL = `g4;    12'd277: toneL = `sil;
                12'd278: toneL = `b4;    12'd279: toneL = `b4;
                12'd280: toneL = `b4;    12'd281: toneL = `b4;
                12'd282: toneL = `b4;    12'd283: toneL = `b4;
                12'd284: toneL = `b4;    12'd285: toneL = `b4;
                12'd286: toneL = `b4;    12'd287: toneL = `sil;
 
                12'd288: toneL = `c5;    12'd289: toneL = `c5;
                12'd290: toneL = `c5;    12'd291: toneL = `c5;
                12'd292: toneL = `c5;    12'd293: toneL = `c5;
                12'd294: toneL = `c5;    12'd295: toneL = `c5;
                12'd296: toneL = `c5;    12'd297: toneL = `c5;
                12'd298: toneL = `c5;    12'd299: toneL = `c5;
                12'd300: toneL = `c5;    12'd301: toneL = `c5;
                12'd302: toneL = `c5;    12'd303: toneL = `sil;
 
                12'd304: toneL = `a4;    12'd305: toneL = `a4;
                12'd306: toneL = `a4;    12'd307: toneL = `a4;
                12'd308: toneL = `a4;    12'd309: toneL = `a4;
                12'd310: toneL = `a4;    12'd311: toneL = `sil;
                12'd312: toneL = `b4;    12'd313: toneL = `b4;
                12'd314: toneL = `b4;    12'd315: toneL = `b4;
                12'd316: toneL = `b4;    12'd317: toneL = `b4;
                12'd318: toneL = `b4;    12'd319: toneL = `sil;
                //Measure 6
                12'd320: toneL = `sil;    12'd321: toneL = `sil;
                12'd322: toneL = `sil;    12'd323: toneL = `sil;
                12'd324: toneL = `sil;    12'd325: toneL = `sil;
                12'd326: toneL = `sil;    12'd327: toneL = `sil;
                12'd328: toneL = `a4;    12'd329: toneL = `a4;
                12'd330: toneL = `a4;    12'd331: toneL = `a4;
                12'd332: toneL = `a4;    12'd333: toneL = `a4;
                12'd334: toneL = `a4;    12'd335: toneL = `a4;
 
                12'd336: toneL = `a4;    12'd337: toneL = `a4;
                12'd338: toneL = `a4;    12'd339: toneL = `a4;
                12'd340: toneL = `a4;    12'd341: toneL = `a4;
                12'd342: toneL = `a4;    12'd343: toneL = `sil;
                12'd344: toneL = `e4;    12'd345: toneL = `e4;
                12'd346: toneL = `e4;    12'd347: toneL = `e4;
                12'd348: toneL = `e4;    12'd349: toneL = `e4;
                12'd350: toneL = `e4;    12'd351: toneL = `sil;
 
                12'd352: toneL = `f4;    12'd353: toneL = `f4;
                12'd354: toneL = `f4;    12'd355: toneL = `f4;
                12'd356: toneL = `f4;    12'd357: toneL = `f4;
                12'd358: toneL = `f4;    12'd359: toneL = `sil;
                12'd360: toneL = `d4;    12'd361: toneL = `d4;
                12'd362: toneL = `d4;    12'd363: toneL = `d4;
                12'd364: toneL = `d4;    12'd365: toneL = `d4;
                12'd366: toneL = `d4;    12'd367: toneL = `d4;
 
                12'd368: toneL = `d4;    12'd369: toneL = `d4;
                12'd370: toneL = `d4;    12'd371: toneL = `d4;
                12'd372: toneL = `d4;    12'd373: toneL = `d4;
                12'd374: toneL = `d4;    12'd375: toneL = `sil;
                12'd376: toneL = `sil;    12'd377: toneL = `sil;
                12'd378: toneL = `sil;    12'd379: toneL = `sil;
                12'd380: toneL = `sil;    12'd381: toneL = `sil;
                12'd382: toneL = `sil;    12'd383: toneL = `sil;
                default : toneL = `sil;
            endcase
        end
        
 		if(en == 1)begin/*gmae over music*/
        case(ibeatNum)
                //Measure 1
                12'd0: toneL = `d5;     12'd1: toneL = `d5;
                12'd2: toneL = `d5;     12'd3: toneL = `d5;
                12'd4: toneL = `d5;     12'd5: toneL = `d5;
                12'd6: toneL = `d5;     12'd7: toneL = `d5;
                12'd8: toneL = `a4;     12'd9: toneL = `a4;
                12'd10: toneL = `a4;    12'd11: toneL = `a4;
                12'd12: toneL = `a4;    12'd13: toneL = `a4;
                12'd14: toneL = `a4;    12'd15: toneL = `a4;
 
                12'd16: toneL = `sf4;    12'd17: toneL = `sf4;
                12'd18: toneL = `sf4;    12'd19: toneL = `sf4;
                12'd20: toneL = `sf4;    12'd21: toneL = `sf4;
                12'd22: toneL = `sf4;    12'd23: toneL = `sf4;
                12'd24: toneL = `d4;    12'd25: toneL = `d4;
                12'd26: toneL = `d4;    12'd27: toneL = `d4;
                12'd28: toneL = `d4;    12'd29: toneL = `d4;
                12'd30: toneL = `d4;    12'd31: toneL = `d4;
                default : toneL = `sil;

			endcase
		end
    end
endmodule