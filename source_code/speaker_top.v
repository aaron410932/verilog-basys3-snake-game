`define silence   32'd50000000

`define WAIT 10'd0;
`define PLAY 10'd1;

module speaker_top(
    clk, // clock from crystal
    rst, // active high reset: BTNC
    game_status,
    audio_mclk, // master clock
    audio_lrck, // left-right clock
    audio_sck, // serial clock
    audio_sdin // serial audio data input
);

    // I/O declaration
    input clk;  // clock from the crystal
    input rst;  // active high reset
    input [1:0] game_status;
    output audio_mclk; // master clock
    output audio_lrck; // left-right clock
    output audio_sck; // serial clock
    output audio_sdin; // serial audio data input
    
    // Modify these
    //assign _led_vol = 5'b1_1111;
    //assign DIGIT = 4'b0001;
    //assign DISPLAY = 7'b111_1111;

    // Internal Signal
    wire diemusic;
    wire [15:0] audio_in_left, audio_in_right;
    wire opvolup, opvoldown;
    wire clkDiv22;
    wire [11:0] ibeatNum; // Beat counter
    wire [31:0] freqL, freqR; // Raw frequency, produced by music module
    wire [21:0] freq_outL, freq_outR; // Processed Frequency, adapted to the clock rate of Basys3
    reg [2:0] volume = 3'd3;
    reg [4:0] myled = 5'b00111;
    reg ismute;
    assign freq_outL = 50000000 / (ismute ? `silence : freqL); // Note gen makes no sound, if freq_out = 50000000 / `silence = 1
    assign freq_outR = 50000000 / (ismute ? `silence : freqR);
    
    m_clock_divider #(.n(21)) clock_22(
        .clk(clk),
        .clk_div(clkDiv22)
    );

    // Player Control
   music_control #(.LEN(383)) playerCtrl_00 ( 
        .clk(clkDiv22),
        .reset(rst),
        .ibeat(ibeatNum),
        .game_status(game_status),
        .diemusic(diemusic)
    );
    // Music module
    // [in]  beat number and en
    // [out] left & right raw frequency
    music_example music_00 (
        .ibeatNum(ibeatNum),
        .en(diemusic),
        .toneL(freqL),
        .toneR(freqR)
    );

    // Note generation
    // [in]  processed frequency
    // [out] audio wave signal (using square wave here)
    note_gen noteGen_00(
        .clk(clk), // clock from crystal
        .rst(rst), // active high reset
        .note_div_left(freq_outL),
        .note_div_right(freq_outR),
        .audio_left(audio_in_left), // left sound audio
        .audio_right(audio_in_right)
    );

    // Speaker controller
    speaker_control sc(
        .clk(clk),  // clock from the crystal
        .rst(rst),  // active high reset
        .audio_in_left(audio_in_left), // left channel audio data input
        .audio_in_right(audio_in_right), // right channel audio data input
        .audio_mclk(audio_mclk), // master clock
        .audio_lrck(audio_lrck), // left-right clock
        .audio_sck(audio_sck), // serial clock
        .audio_sdin(audio_sdin) // serial audio data input
    );

endmodule
module m_clock_divider(clk, clk_div);   
    parameter n = 26;     
    input clk;   
    output clk_div;   
    
    reg [n-1:0] num;
    wire [n-1:0] next_num;
    
    always@(posedge clk)begin
    	num<=next_num;
    end
    
    assign next_num = num +1;
    assign clk_div = num[n-1];
    
endmodule